//
//  User.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/29/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * goldCount;
@property (nonatomic, retain) NSString * gameCenterId;
@property (nonatomic, retain) NSSet *players;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addPlayersObject:(Player *)value;
- (void)removePlayersObject:(Player *)value;
- (void)addPlayers:(NSSet *)values;
- (void)removePlayers:(NSSet *)values;

@end
