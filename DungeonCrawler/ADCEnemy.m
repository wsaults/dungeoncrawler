//
//  ADCEnemy.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCEnemy.h"
#import "ADCLootDropManager.h"
#import "ADCTextureManager.h"
#import "ADCUtilities.h"

@interface ADCEnemy ()

@property (nonatomic, strong) SKAction *enemyDied;
@property (nonatomic, strong) SKAction *enemyAttack;

@end

@implementation ADCEnemy

- (instancetype)initWithPosition:(CGPoint)position withType:(int)type {
    NSString *textureName = [NSString stringWithFormat:@"monster_%d", type];
    SKTexture *texture = [[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:textureName];
    self = [super initWithTexture:texture];
    
    if (self) {
        self.position = position;
        self.anchorPoint = CGPointMake(.5, .5);
        self.enemyType = type;
        
        NSString *fileName = [NSString stringWithFormat:@"enemy_die_%d.wav", self.enemyType];
        self.enemyDied = [SKAction playSoundFileNamed:fileName waitForCompletion:NO];
        
        fileName = [NSString stringWithFormat:@"enemy_attack_%d.wav", self.enemyType];
        self.enemyAttack = [SKAction playSoundFileNamed:fileName waitForCompletion:NO];
    }
    
    return self;
}

- (void)startChase
{
    NSLog(@"chasing");
}

- (void)stopChase
{
    
}

- (void)die {
    [[ADCLootDropManager sharedManager] askForItemDropAtPosition:self.position];
    [[ADCLootDropManager sharedManager] askForGoldDropAtPosition:self.position];
    [[ADCLootDropManager sharedManager] askForPotionDropAtPosition:self.position];
    
    if (random_int(0, 4) == 1) {
        [self.scene runAction:self.enemyDied];
    }
    
    [self removeFromParent];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EnemyDied" object:self];
}

- (void)attackSound
{
    // Random chance enemy will groan when attacking
    if (random_int(0, 6) == 1) {
        [self.scene runAction:self.enemyAttack];
    }
}

- (void)attackAnimationTowardCharacter:(ADCCharacter *)character
{
    // TODO: while player is attacking play a sound
    
    //        float deltaX = character.position.x - self.position.x;
    //        float deltaY = character.position.y - self.position.y;
    //        float angle = atan2f(deltaY, deltaX);
    
    
    NSString *textureName = [NSString stringWithFormat:@"monster_%d", 0]; // TODO: enemy attack image type.
    SKSpriteNode *attackNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] attacksAtlas] textureNamed:textureName]];
    //        [attackNode setAlpha:0];
    [attackNode setPosition:self.position];
    //        [attackNode setAnchorPoint:CGPointMake(.5, 0)];
    //        [attackNode setZRotation:angle];
    [self.parent addChild:attackNode];
    
    SKAction *fade = [SKAction fadeAlphaTo:1 duration:.2];
    SKAction *move = [SKAction moveTo:character.position duration:.2];
    
    SKAction *group = [SKAction group:@[fade, move]];
    SKAction *block = [SKAction runBlock:^{
        [attackNode removeFromParent];
    }];
    SKAction *attackSequence = [SKAction sequence:@[group, block]];
    [attackNode runAction:attackSequence];
}

@end
