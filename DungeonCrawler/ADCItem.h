//
//  ADCItem.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "ADCLoot.h"

typedef enum {
    WEAPON      = 0,
    OFF_HAND    = 1,
    HEAD_GEAR   = 2
} kClassType;

@interface ADCItem : ADCLoot

@property (nonatomic, assign) NSInteger sellValue;
@property (nonatomic, assign) NSInteger vitality;
@property (nonatomic, assign) NSInteger damage;
@property (nonatomic, assign) CGFloat attackRate;
@property (nonatomic, assign) NSInteger defense;
@property (nonatomic, assign) NSInteger classType;
@property (nonatomic, strong) NSString *nameOfItem;
@property (nonatomic) BOOL canBePickedUp;

- (id)initWithTexture:(SKTexture *)texture andClassType:(NSInteger)classType;
+ (NSString *)typeNameFromClassType:(NSInteger)classType;
- (void)generateItemStatsBasedOnPlayerLvl:(NSInteger)lv;

@end
