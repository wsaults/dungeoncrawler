//
//  ADCViewController.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCViewController.h"
#import "ADCMyScene.h"
#import "ADCItemPickupViewController.h"
#import "ADCItem.h"
#import "Player.h"
#import <AVFoundation/AVFoundation.h>

@interface ADCViewController ()

@property (nonatomic, strong) ADCItem *itemPickedUp;
@property (nonatomic, strong) ADCMyScene *scene;

@property (nonatomic, weak) IBOutlet UITextView *mapTextView;
@property (nonatomic, strong) UIImageView *playerMapImageView;

// Pause view
@property (nonatomic, weak) IBOutlet UIView *pauseView;
@property (nonatomic, weak) IBOutlet UIButton *resumeButton;

// Skill bar
@property (nonatomic, weak) IBOutlet UIButton *flameSkillButton;
@property (nonatomic, weak) IBOutlet UILabel *flameSkillCooldownLabel;

@property (nonatomic, strong) NSTimer *flameTimer;
@property (nonatomic, assign) NSUInteger flameCounter;

@property (nonatomic, strong) AVAudioPlayer *backgroundMusicPlayer;

@end

@implementation ADCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
//    skView.showsFPS = YES;
//    skView.showsNodeCount = YES;
    
    self.mapTextView.hidden = YES;
    self.mapTextView.alpha = 0;
    [[NSNotificationCenter defaultCenter] addObserverForName:@"setMap" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSString *mapText = (NSString *)note.object;
        mapText = [mapText stringByReplacingOccurrencesOfString:@"1"withString:@"0"];
        mapText = [mapText stringByReplacingOccurrencesOfString:@"2"withString:@"Z"];
        mapText = [mapText stringByReplacingOccurrencesOfString:@"3"withString:@"H"];
        
        NSMutableString *reversedString = [NSMutableString string];
        NSInteger charIndex = [mapText length];
        while (charIndex > 0) {
            charIndex--;
            NSRange subStrRange = NSMakeRange(charIndex, 1);
            [reversedString appendString:[mapText substringWithRange:subStrRange]];
        }
        
        [self.mapTextView setText:reversedString];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"updateMiniMapPosition" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note)
    {
//        NSValue *positionValue = (NSValue *)note.object;
//        CGPoint position = [positionValue CGPointValue];
//     
//        position = [self.view convertPoint:position toView:self.mapTextView];
//        [self.playerMapImageView setFrame:CGRectMake(position.x, position.y, self.playerMapImageView.frame.size.width / 4, self.playerMapImageView.frame.size.height / 4)];
    }];

    // Create and configure the scene.
    self.scene = [ADCMyScene sceneWithSize:skView.bounds.size];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:self.scene];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"itemPickup" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        self.itemPickedUp = (ADCItem *)note.object;
        
        [self performSegueWithIdentifier:@"itemPickupSegue" sender:self];
    }];
    
    self.pauseView.hidden = YES;
    self.pauseView.alpha = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pause:) name:@"togglePauseMenu" object:nil];
    
//    self.playerMapImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"player"]];
//    [self.view addSubview:self.playerMapImageView];
    
    self.flameCounter = 0;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flameSkill"] isEqualToNumber:@1]) {
        [self.flameSkillButton setHidden:NO];
    }
    
    NSError *error;
    NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"game_bg_music" withExtension:@"wav"];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.backgroundMusicPlayer.numberOfLoops = -1;
    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer play];
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    // TODO: fix bug when modal view appears.
    [super viewWillDisappear:animated];
    if (self.flameTimer!=nil)
    {
        [self.flameTimer invalidate]; //need to invalidate in order to release this controller and views from memory
        self.flameTimer = nil;
    }
    
    [self fadeVolume];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unpause" object:nil];
}

- (IBAction)toggleMap:(id)sender
{
    if (!self.mapTextView.hidden) {
        // Fade out
        [UIView animateWithDuration:.3 animations:^{
            self.mapTextView.alpha = 0;
        } completion:^(BOOL finished) {
            self.mapTextView.hidden = YES;
        }];
    } else {
        // Fade in
        self.mapTextView.hidden = NO;
        [UIView animateWithDuration:.3 animations:^{
            self.mapTextView.alpha = 1;
        }];
    }
}

- (IBAction)pause:(id)sender
{
    self.resumeButton.hidden = [self.scene.savedPlayer.is_alive  isEqual:@0] ? YES : NO;
    
    if (!self.pauseView.hidden) {
        // Fade out
        [UIView animateWithDuration:.3 animations:^{
            self.pauseView.alpha = 0;
        } completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"unpause" object:nil];
            self.pauseView.hidden = YES;
        }];
    } else {
        // Fade in
        self.pauseView.hidden = NO;
        [UIView animateWithDuration:.3 animations:^{
            self.pauseView.alpha = 1;
        } completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pause" object:nil];
        }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"itemPickupSegue"])
    {
        ADCItemPickupViewController *vc = [segue destinationViewController];
        
        [vc setItemPickedUp:self.itemPickedUp];
    } else if ([[segue identifier] isEqualToString:@"inventorySegue"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pause" object:nil];
    }
        
}

#pragma mark - Skills
- (IBAction)flameSkill:(id)sender
{
    if (self.flameCounter > 0) return;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"flameSkill" object:nil];
    
    [self.flameSkillCooldownLabel setHidden:NO];
    [self.flameSkillCooldownLabel setText:[NSString stringWithFormat:@"%d", 20]];
    
    // Initialize the pollingTimer and add it to the mainRunLoop with a "common" mode.
    // This allows the timer to continue to poll while the user is scrolling the collection view.
    self.flameTimer = [NSTimer timerWithTimeInterval:1.0
                                           target:self
                                         selector:@selector(decrementCoolDownTimers)
                                         userInfo:nil
                                          repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.flameTimer forMode:NSRunLoopCommonModes];
}

- (void)decrementCoolDownTimers
{
    if (self.flameCounter >= 20) {
        [self.flameTimer invalidate];
        self.flameTimer = nil;
        self.flameCounter = 0;
        [self.flameSkillCooldownLabel setHidden:YES];
    } else {
        [self.flameSkillCooldownLabel setText:[NSString stringWithFormat:@"%d", 20 - (int)++self.flameCounter]];
    }
}

#pragma mark - Fade
// TODO: put music methods into a singleton class and do something like
// - (void)fadeAudioPlayer:(AVAudioPlayer *)player
-(void)fadeVolume
{
    if (self.backgroundMusicPlayer.volume > 0.1) {
        self.backgroundMusicPlayer.volume = self.backgroundMusicPlayer.volume - 0.1;
        [self performSelector:@selector(fadeVolume) withObject:nil afterDelay:0.1];
    } else {
        // Stop and get the sound ready for playing again
        [self.backgroundMusicPlayer stop];
        self.backgroundMusicPlayer.currentTime = 0;
        [self.backgroundMusicPlayer prepareToPlay];
        self.backgroundMusicPlayer.volume = 1.0;
    }
}

@end
