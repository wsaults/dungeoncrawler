//
//  ADCCharacter.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCCharacter.h"
#import "ADCUtilities.h"

#define POLAR_ADJUST(x) x + (M_PI * 0.5f)

@implementation ADCCharacter

//- (instancetype)initWithTexture:(SKTexture *)texture andClassType:(NSInteger)classType
//{
//    self = [super initWithTexture:texture];
//    if (self) {
//
//    }
//    
//    return self;
//}

- (void)attackCharacter:(ADCCharacter *)character
{
    // If you're not already attacking then dish out some damage.
    if (!self.isAttacking) {
        self.attacking = YES;
        
        [self attackSound];
        [self attackAnimationTowardCharacter:character];
        
        [character receiveDamage:self.damage];
        
        // Wait for the character's attackrate delay.
        SKAction *wait = [SKAction waitForDuration:self.attackRate];
        SKAction *waitBlock = [SKAction runBlock:^{
            self.attacking = NO;
        }];
        
        SKAction *sequence = [SKAction sequence:@[wait, waitBlock]];
        [self runAction:sequence];
    }
}

- (void)attackSound
{
    // Overridden by subclass
}

- (void)attackAnimationTowardCharacter:(ADCCharacter *)character
{
    // Overridden by subclass
}

- (void)moveSound
{
    // Overridden by subclass
}

- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval
{
    [self moveSound];
    
    CGPoint curPosition = self.position;
    CGFloat dx = position.x - curPosition.x;
    CGFloat dy = position.y - curPosition.y;
//    CGFloat dt = self.movementSpeed * timeInterval;
    CGFloat dt = 50 * timeInterval;
    
    CGFloat ang = POLAR_ADJUST(RadiansBetweenPoints(position, curPosition));
    
    CGFloat distRemaining = hypotf(dx, dy);
    if (distRemaining < dt) {
        self.position = position;
    } else {
        self.position = CGPointMake(curPosition.x - sinf(ang)*dt,
                                    curPosition.y + cosf(ang)*dt);
    }
}

- (void)die {
    // Overidden by subclass
}

- (void)receiveDamage:(CGFloat)damage
{
    damage -= ((self.defense / 10.0f) * damage);
    self.vitality -= damage;
    if (self.vitality <= 0) {
        self.vitality = 0;
        [self die];
    }
}

@end
