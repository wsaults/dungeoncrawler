//
//  Sheild.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/28/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface Sheild : NSManagedObject

@property (nonatomic, retain) NSNumber * defense;
@property (nonatomic, retain) NSString * item_name;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) NSNumber * vitality;
@property (nonatomic, retain) Player *player;

@end
