//
//  Helm.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Helm.h"
#import "Player.h"


@implementation Helm

@dynamic defense;
@dynamic vitality;
@dynamic item_name;
@dynamic value;
@dynamic player;

@end
