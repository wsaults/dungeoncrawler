//
//  Player.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Player.h"
#import "Helm.h"
#import "Sheild.h"
#import "User.h"
#import "Weapon.h"


@implementation Player

@dynamic attack_rate;
@dynamic defense;
@dynamic experiance;
@dynamic farthestFloorReached;
@dynamic level;
@dynamic movement_speed;
@dynamic playerId;
@dynamic spendable_points;
@dynamic strength;
@dynamic vitality;
@dynamic first_name;
@dynamic last_name;
@dynamic is_alive;
@dynamic helm;
@dynamic shield;
@dynamic user;
@dynamic weapon;

@end
