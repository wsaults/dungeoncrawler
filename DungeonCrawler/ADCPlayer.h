//
//  ADCPlayer.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCCharacter.h"

@interface ADCPlayer : ADCCharacter

@property (nonatomic, getter = isDying) BOOL dying;

- (void)updatePosition:(CGPoint)position;

// TODO: player death: screen goes dark and the player must do some action (tapping and etc.) to resurrect before death leaves with your soul and it's game over.
// additional option is to bribe death with some gold for immediate revival.

@end
