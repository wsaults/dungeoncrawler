//
//  ADCModelManager.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/6/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCModelManager.h"
#import "User.h"
#import "Player.h"
#import "Weapon.h"
#import "Shield.h"
#import "Helm.h"
#import "ADCItem.h"
#import "ADCNameGenerator.h"

@implementation ADCModelManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (instancetype)sharedManager
{
    static ADCModelManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (instancetype)init
{
    if (self = [super init]) {
    }
    return self;
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Local database does not match the current version of the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         */
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [[ADCModelManager sharedManager] managedObjectContext];
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort(); // TODO: abort should be removed before shipping app.
        }
    }
}

- (NSManagedObject *)createEntity:(NSString *)entity
{
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:self.managedObjectContext];
}

- (void)deleteEntity:(NSManagedObject*)entity
{
    [[self managedObjectContext] deleteObject:entity];
}

- (id)getEntity:(NSString*)entity
{
    NSManagedObjectContext *context = [[ADCModelManager sharedManager] managedObjectContext];
    if (context == nil) return nil;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entity];
    [fetch setFetchLimit:1];
    
    NSError *fetchError;
    NSArray *results = [context executeFetchRequest:fetch error:&fetchError];
    return results.count == 0 ? nil : [results objectAtIndex:0];
}

- (id)getEntity:(NSString*)entity withPredicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = [[ADCModelManager sharedManager] managedObjectContext];
    if (context == nil) return nil;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entity];
    [fetch setFetchLimit:1];
    if (predicate != nil) [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *results = [context executeFetchRequest:fetch error:&fetchError];
    return results.count == 0 ? nil : [results objectAtIndex:0];
}


#pragma mark - User

- (User *)createUserWithGameCenterId:(NSString *)gameCenterId
{
    User * user = (User *)[[ADCModelManager sharedManager] createEntity:@"User"];
    [user setGoldCount:@0];
    [user setGameCenterId:gameCenterId];
    return user;
}

- (User *)userWithGameCenterId:(NSString *)gameCenterId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gameCenterId LIKE %@", gameCenterId];
    User *user = [[ADCModelManager sharedManager] getEntity:@"User" withPredicate:predicate];
    if (user == nil) {
        user = [self createUserWithGameCenterId:gameCenterId];
        [self saveContext];
    }
    
    return user;
}

#pragma mark - Player

// To-many relationships
// https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CoreData/Articles/cdUsingMOs.html

- (Player *)createPlayer
{
    Player *player = (Player *)[[ADCModelManager sharedManager] createEntity:@"Player"];
    // Add temporary base stats
    [player setFarthestFloorReached:@1];
    [player setVitality:[NSNumber numberWithFloat:100.0f]];
    [player setStrength:[NSNumber numberWithFloat:1.0f]];
    [player setAttack_rate:[NSNumber numberWithFloat:.5f]];
    [player setDefense:[NSNumber numberWithFloat:0.0f]];
    [player setMovement_speed:[NSNumber numberWithFloat:100.0f]];
    [player setExperiance:@0];
    [player setIs_alive:[NSNumber numberWithBool:YES]];
    
    NSDictionary *nameDict = [[ADCNameGenerator shared] generateFullName];
    [player setFirst_name:[nameDict valueForKey:kFirstName]];
    [player setLast_name:[nameDict valueForKey:kLastName]];

    [player setLevel:@1]; // Requried exp to level up = x^2 * (100 * x) | x = current level.
    return player;
}

- (Player *)addPlayerToUser:(User *)user
{
    NSNumber *playerCount = [NSNumber numberWithInteger:[self playersCountForUser:user]];
    
    Player *player = [self createPlayer];
    [player setPlayerId:playerCount];
    
    NSSet *newPlayers = [NSSet setWithObjects:player, nil];
    [user addPlayers:newPlayers];
    [self saveContext];
    
    return player;
}

- (NSArray *)playersForUser:(User *)user
{
    return user.players.allObjects;
}

- (NSUInteger)playersCountForUser:(User *)user
{
    return user.players.allObjects.count;
}

- (Player *)playerWithId:(NSUInteger)playerId forUser:(User *)user
{
    NSSet *players = user.players;
    
    Player *player = nil;
    for (Player *p in players) {
        if (p.playerId.integerValue == playerId) {
            player = p;
        }
    }
    
    return player;
}

- (void)levelUpPlayer:(Player *)player
{
    [player setLevel:[NSNumber numberWithInteger:player.level.integerValue + 1]];
    [player setSpendable_points:[NSNumber numberWithInteger:player.spendable_points.integerValue + 1]];
    [[ADCModelManager sharedManager] saveContext];
}

#pragma mark - Weapon
- (Weapon *)createWeaponForPlayer:(Player *)player
{
    Weapon *weapon = (Weapon *)[[ADCModelManager sharedManager] createEntity:@"Weapon"];
    [weapon setPlayer:player];
    [self saveContext];
    return weapon;
}

- (void)setWeapon:(ADCItem *)item forPlayer:(Player *)player
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"player.playerId LIKE %@", player.playerId.stringValue];
    Weapon *weapon = [[ADCModelManager sharedManager] getEntity:@"Weapon" withPredicate:predicate];
    if (weapon == nil) {
        weapon = [self createWeaponForPlayer:player];
    }
    
    [weapon setStrength:[NSNumber numberWithFloat:item.damage]];
    [weapon setVitality:[NSNumber numberWithFloat:item.vitality]];
    [weapon setAttack_rate:[NSNumber numberWithFloat:item.attackRate]];
    [weapon setItem_name:item.nameOfItem];
    [weapon setValue:[NSNumber numberWithInteger:item.sellValue]];
    
    [self saveContext];
}

#pragma mark - Helm
- (Helm *)createHelmForPlayer:(Player *)player
{
    Helm *helm = (Helm *)[[ADCModelManager sharedManager] createEntity:@"Helm"];
    [helm setPlayer:player];
    [self saveContext];
    return helm;
}

- (void)setHelm:(ADCItem *)item forPlayer:(Player *)player
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"player.playerId LIKE %@", player.playerId.stringValue];
    Helm *helm = [[ADCModelManager sharedManager] getEntity:@"Helm" withPredicate:predicate];
    if (helm == nil) {
        helm = [self createHelmForPlayer:player];
    }
    
    [helm setDefense:[NSNumber numberWithFloat:item.defense]];
    [helm setVitality:[NSNumber numberWithFloat:item.vitality]];
    [helm setItem_name:item.nameOfItem];
    [helm setValue:[NSNumber numberWithInteger:item.sellValue]];
    
    [self saveContext];
}

#pragma mark - Shield
- (Shield *)createShieldForPlayer:(Player *)player
{
    Shield *shield = (Shield *)[[ADCModelManager sharedManager] createEntity:@"Shield"];
    [shield setPlayer:player];
    [self saveContext];
    return shield;
}

- (void)setShield:(ADCItem *)item forPlayer:(Player *)player
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"player.playerId LIKE %@", player.playerId.stringValue];
    Shield *shield = [[ADCModelManager sharedManager] getEntity:@"Shield" withPredicate:predicate];
    if (shield == nil) {
        shield = [self createShieldForPlayer:player];
    }
    
    [shield setDefense:[NSNumber numberWithFloat:item.defense]];
    [shield setVitality:[NSNumber numberWithFloat:item.vitality]];
    [shield setItem_name:item.nameOfItem];
    [shield setValue:[NSNumber numberWithInteger:item.sellValue]];
    
    [self saveContext];
}

@end
