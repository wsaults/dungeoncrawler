//
//  Player.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Helm, Sheild, User, Weapon;

@interface Player : NSManagedObject

@property (nonatomic, retain) NSNumber * attack_rate;
@property (nonatomic, retain) NSNumber * defense;
@property (nonatomic, retain) NSNumber * experiance;
@property (nonatomic, retain) NSNumber * farthestFloorReached;
@property (nonatomic, retain) NSNumber * level;
@property (nonatomic, retain) NSNumber * movement_speed;
@property (nonatomic, retain) NSNumber * playerId;
@property (nonatomic, retain) NSNumber * spendable_points;
@property (nonatomic, retain) NSNumber * strength;
@property (nonatomic, retain) NSNumber * vitality;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSNumber * is_alive;
@property (nonatomic, retain) Helm *helm;
@property (nonatomic, retain) Sheild *shield;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) Weapon *weapon;

@end
