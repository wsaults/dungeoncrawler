//
//  ADCItemDropManager.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/2/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADCItem;
@class ADCGold;
@class ADCPotion;

@interface ADCLootDropManager : NSObject

+ (instancetype)sharedManager;

- (void)askForItemDropAtPosition:(CGPoint)location;
- (void)askForGoldDropAtPosition:(CGPoint)location;
- (void)askForPotionDropAtPosition:(CGPoint)location;
- (ADCItem *)item;
- (ADCGold *)gold;
- (ADCPotion *)potion;

@end
