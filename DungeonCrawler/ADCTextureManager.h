//
//  ADCTextureManager.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/3/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface ADCTextureManager : NSObject

@property (nonatomic, strong) SKTextureAtlas *spriteAtlas;
@property (nonatomic, strong) SKTextureAtlas *itemsAtlas;
@property (nonatomic, strong) SKTextureAtlas *attacksAtlas;

+ (instancetype)sharedManager;

@end
