//
//  ADCInventoryViewController.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/27/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCInventoryViewController.h"
#import "ADCModelManager.h"
#import "User.h"
#import "Player.h"
#import "Weapon.h"
#import "Sheild.h"
#import "Helm.h"

typedef enum {
    VITALITY_STAT = 0,
    DEFENSE_STAT,
    STRENGTH_STAT,
    ATTACK_RATE_STAT
} kStats;

@interface ADCInventoryViewController ()

#pragma mark - Weapon
@property (nonatomic, weak) IBOutlet UILabel *weaponName;
@property (nonatomic, weak) IBOutlet UITextView *weaponText;

#pragma mark - Helm
@property (nonatomic, weak) IBOutlet UILabel *helmName;
@property (nonatomic, weak) IBOutlet UITextView *helmText;

#pragma mark - Off hand
@property (nonatomic, weak) IBOutlet UILabel *offHandName;
@property (nonatomic, weak) IBOutlet UITextView *offHandText;

#pragma mark - Player stats
@property (nonatomic, weak) IBOutlet UILabel *vitality;
@property (nonatomic, weak) IBOutlet UILabel *defense;
@property (nonatomic, weak) IBOutlet UILabel *strength;
@property (nonatomic, weak) IBOutlet UILabel *attackRate;

@property (nonatomic, weak) IBOutlet UILabel *vitalityValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *defenseValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *strengthValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *attackRateValueLabel;

@property (nonatomic, weak) IBOutlet UIButton *increaseVitalityStat;
@property (nonatomic, weak) IBOutlet UIButton *increaseDefenseStat;
@property (nonatomic, weak) IBOutlet UIButton *increaseStrengthStat;
@property (nonatomic, weak) IBOutlet UIButton *increaseAttackRateStat;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Player *player;

@end

@implementation ADCInventoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Combine player stats and weapon stats.
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    self.user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    self.player = [[ADCModelManager sharedManager] playerWithId:0 forUser:self.user];
    
    [self updateStatsUI];
    
    [self.weaponName setText:self.player.weapon.item_name];
    [self.helmName setText:self.player.helm.item_name];
    [self.offHandName setText:self.player.shield.item_name];
    
    // Set weapon text
    NSInteger vitality = self.player.weapon.vitality.integerValue;
    NSInteger damage = self.player.weapon.strength.integerValue;
    CGFloat attackRate = self.player.weapon.attack_rate.floatValue;
    NSInteger defense = 0;
    
    NSString *damageString = (damage == 0) ? @"" : [NSString stringWithFormat:@"Damage: %d\n", (int)damage];
    NSString *attackRateString = (attackRate == 0) ? @"" : [NSString stringWithFormat:@"Attack Rate: %d\n", (int)attackRate];
    NSString *vitalityString = (vitality == 0) ? @"" : [NSString stringWithFormat:@"Vitality: %d\n", (int)vitality];
    NSString *defenseString = @"";
    
    NSString *statsString = [NSString stringWithFormat:@"%@%@%@", damageString, attackRateString, vitalityString];
    [self.weaponText setText:statsString];
    
    // Set helm text
    defense = self.player.helm.defense.integerValue;
    vitality = self.player.helm.vitality.integerValue;
    defenseString = (defense == 0) ? @"" : [NSString stringWithFormat:@"Defense: %d\n", (int)defense];
    vitalityString = (vitality == 0) ? @"" : [NSString stringWithFormat:@"Vitality: %d\n", (int)vitality];
    
    statsString = [NSString stringWithFormat:@"%@%@", defenseString, vitalityString];
    [self.helmText setText:statsString];
    
    // Set shield text
    defense = self.player.shield.defense.integerValue;
    vitality = self.player.shield.vitality.integerValue;
    defenseString = (defense == 0) ? @"" : [NSString stringWithFormat:@"Defense: %d\n", (int)defense];
    vitalityString = (vitality == 0) ? @"" : [NSString stringWithFormat:@"Vitality: %d\n", (int)vitality];
    
    statsString = [NSString stringWithFormat:@"%@%@", defenseString, vitalityString];
    [self.offHandText setText:statsString];
    
    if (self.player.spendable_points.integerValue > 0) {
        [self shouldHideStatButtons:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unpause" object:nil];
}

- (void)updateStatsUI
{
    int vitalityNumber = self.player.vitality.intValue + self.player.weapon.vitality.intValue +
    self.player.helm.vitality.intValue + self.player.shield.vitality.intValue;
    
    int defenseNumber = self.player.defense.intValue + self.player.helm.defense.intValue + self.player.shield.defense.intValue;
    
    [self.vitality setText:[NSString stringWithFormat:@"%d", vitalityNumber]];
    [self.defense setText:[NSString stringWithFormat:@"%d", defenseNumber]];
    [self.strength setText:[NSString stringWithFormat:@"%d", self.player.strength.intValue + self.player.weapon.strength.intValue]];
    [self.attackRate setText:[NSString stringWithFormat:@"%.02f", self.player.attack_rate.floatValue + self.player.weapon.attack_rate.intValue]];
}

- (IBAction)increaseStat:(UIButton *)sender
{
    switch (sender.tag) {
        case VITALITY_STAT:
            [self.player setVitality:[NSNumber numberWithInteger:self.player.vitality.integerValue + 10]];
            break;
            
        case DEFENSE_STAT:
            [self.player setDefense:[NSNumber numberWithInteger:self.player.defense.integerValue + 1]];
            break;
            
        case STRENGTH_STAT:
            [self.player setStrength:[NSNumber numberWithInteger:self.player.strength.integerValue + 1]];
            break;
            
        case ATTACK_RATE_STAT:
            [self.player setAttack_rate:[NSNumber numberWithFloat:self.player.attack_rate.floatValue + .10]];
            break;
            
        default:
            break;
    }
    
    [self.player setSpendable_points:[NSNumber numberWithInteger:self.player.spendable_points.integerValue - 1]];
    [[ADCModelManager sharedManager] saveContext];
    
    [self updateStatsUI];
    
    if (self.player.spendable_points.integerValue == 0) {
        // Hide stat buttons
        [self shouldHideStatButtons:YES];
    }
}

- (void)shouldHideStatButtons:(BOOL)isHidden
{
    [self.increaseVitalityStat setHidden:isHidden];
    [self.increaseDefenseStat setHidden:isHidden];
    [self.increaseStrengthStat setHidden:isHidden];
    [self.increaseAttackRateStat setHidden:isHidden];
    
    [self.vitalityValueLabel setHidden:isHidden];
    [self.defenseValueLabel setHidden:isHidden];
    [self.strengthValueLabel setHidden:isHidden];
    [self.attackRateValueLabel setHidden:isHidden];
}

@end
