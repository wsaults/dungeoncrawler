//
//  ADCMap.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCMap.h"
#import "ADCMapTiles.h"
#import "ADCMyScene.h"
#import "ADCUtilities.h"

@interface ADCMap ()

@property (nonatomic, strong) SKTextureAtlas *tileAtlas;
@property (nonatomic, assign) CGFloat tileSize;
@property (nonatomic, strong) NSMutableArray *floorMakers;
@property (nonatomic, strong) NSArray *rooms;
@property (nonatomic, strong) NSMutableArray *splitRooms;

@end

@implementation ADCMap

+ (instancetype)mapWithGridSize:(CGSize)gridSize
{
    return [[self alloc] initWithGridSize:gridSize];
}

- (void)generateLevel:(NSUInteger)worldLevel
{
    self.tiles = [[ADCMapTiles alloc] initWithGridSize:self.gridSize];
    self.rooms = @[];
    self.splitRooms = [[NSMutableArray alloc] initWithArray:@[]];
    [self generateTileGrid];
    [self generateDoorway];
    [self generateWalls];
    [self generateDoors];
    [self generateEnemies];
    [self generateTiles];
    [self generateCollisionWalls];
    
    NSLog(@"%@", [self.tiles description]);
}

- (instancetype)initWithGridSize:(CGSize)gridSize
{
    if ((self = [super init]))
    {
        self.gridSize = gridSize;
        _spawnPoint = CGPointZero;
        _exitPoint = CGPointZero;
        
        self.tileAtlas = [SKTextureAtlas atlasNamed:@"tiles"];
        
        NSArray *textureNames = [self.tileAtlas textureNames];
        SKTexture *tileTexture = [self.tileAtlas textureNamed:(NSString *)[textureNames firstObject]];
        self.tileSize = tileTexture.size.width;
    }
    return self;
}

- (void)generateTileGrid // Using Basic BSP Dungeon generation
{
    // http://www.roguebasin.com/index.php?title=Basic_BSP_Dungeon_generation
    
    /*
     We start with a rectangular dungeon filled with wall cells. 
     We are going to split this dungeon recursively until each sub-dungeon has approximately the size of a room.
     
     The dungeon splitting uses this operation :
     
    choose a random direction : horizontal or vertical splitting
    choose a random position (x for vertical, y for horizontal)
    split the dungeon into two sub-dungeons
     */
    
    
    // Make 2 rooms
    [self splitRoom:CGRectMake(0, 0, self.tiles.gridSize.width, self.tiles.gridSize.height)];
    
    self.rooms = [NSArray arrayWithArray:self.splitRooms];
    [self.splitRooms removeAllObjects];
    
    
    // Make 16 rooms total
    int splitCount = 4;
    for (int i = 0; i < splitCount; i++) {
        for (NSValue *room in self.rooms) {
            [self splitRoom:[room CGRectValue]];
        }
        
        self.rooms = [NSArray arrayWithArray:self.splitRooms];
        [self.splitRooms removeAllObjects];
    }
    
    // Create the room sizes
    for (NSValue *room in self.rooms) {
        NSLog(@"room %@", NSStringFromCGRect(room.CGRectValue));
        
//            NSUInteger roomSizeX = [self randomNumberBetweenMin:3
//                                                         andMax:room.CGRectValue.size.width];
//            NSUInteger roomSizeY = [self randomNumberBetweenMin:3
//                                                         andMax:room.CGRectValue.size.height];
    
        NSUInteger roomSizeX = room.CGRectValue.size.width - 1;
        NSUInteger roomSizeY = room.CGRectValue.size.height - 1;
        
        [self generateRoomAt:CGPointMake(room.CGRectValue.origin.x + 1, room.CGRectValue.origin.y + 1)
                    withSize:CGSizeMake(roomSizeX, roomSizeY)];
    }
}

- (void)splitRoom:(CGRect)room
{
    // choose a random direction
    NSInteger direction = random_int(0, 2); // 0 = x, 1 = y
    
    // Remove some randomness to decrease the number of long skinny rooms.
    if (room.size.width < room.size.height && room.size.width <= 12) {
        direction = 1;
    }
    
    if (room.size.height < room.size.width && room.size.height <= 8) {
        direction = 0;
    }
    
    NSLog(@"direction %d", (int)direction);
    
    NSInteger position = direction == 0 ? room.size.width * .45 : room.size.height * .45;
    
    NSLog(@"position %d", (int)position);
    NSLog(@"==========================");
    
    NSInteger x = room.origin.x;
    NSInteger y = room.origin.y;
    CGFloat widthA = room.size.width;
    CGFloat heightA = room.size.height;
    CGFloat widthB = room.size.width;
    CGFloat heightB = room.size.height;
    
    if (direction == 0) {
        x = position > x ? position : x + position;
        widthA = position;
        widthB = room.size.width - position;
    } else {
        y = position > y ? position : y + position;
        heightA = position;
        heightB = room.size.height - position;
    }
    
    CGRect roomA = CGRectMake(room.origin.x, room.origin.y, widthA, heightA);
    CGRect roomB = CGRectMake(x, y, widthB, heightB);
    
    [self.splitRooms addObjectsFromArray:@[[NSValue valueWithCGRect:roomA], [NSValue valueWithCGRect:roomB]]];
}

- (void)generateTiles
{
    // Two for loops, one for x and one for y, iterate through each tile in the grid.
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        for (NSInteger x = 0; x < self.tiles.gridSize.width; x++)
        {
            // This converts the current x- and y-values into a CGPoint structure for the position of the tile within the grid.
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // Here the code determines the type of tile at this position within the grid.
            MapTileType tileType = [self.tiles tileTypeAt:tileCoordinate];
            
            // If the tile type is not an empty tile, then the code proceeds with creating the tile.
            if (tileType != MapTileTypeNone)
            {
                // Based on the tile type, the code loads the respective tile texture from the texture atlas and assigns it to a SKSpriteNode object.
                SKTexture *tileTexture = [self.tileAtlas textureNamed:[NSString stringWithFormat:@"%i", (int)tileType]];
                SKSpriteNode *tile = [SKSpriteNode spriteNodeWithTexture:tileTexture];
                
                // The code sets the position of the tile to the tile coordinate.
                tile.position = [self convertMapCoordinateToWorldCoordinate:CGPointMake(tileCoordinate.x, tileCoordinate.y)];
                
                // Then it adds the created tile node as a child of the map object.
                // This is done to ensure proper scrolling by grouping the tiles to the map where they belong.
                [self addChild:tile];
            }
        }
    }
    
    // Set spawn point
    do {
        for (NSInteger y = 2; y < self.tiles.gridSize.height - 2; y++)
        {
            for (NSInteger x = 2; x < self.tiles.gridSize.width - 2; x++)
            {
                CGPoint tileCoordinate = CGPointMake(x, y);
                MapTileType tileType = [self.tiles tileTypeAt:tileCoordinate];
                if (tileType == MapTileTypeFloor && random_int(0, 1000) == 7) {
                    _spawnPoint = [self convertMapCoordinateToWorldCoordinate:CGPointMake(tileCoordinate.x, tileCoordinate.y)];
//                    _exitPoint = [self convertMapCoordinateToWorldCoordinate:CGPointMake(tileCoordinate.x - 1, tileCoordinate.y - 1)];
                }
            }
        }
    } while (CGPointEqualToPoint(self.spawnPoint, CGPointZero));
    
     // Set exit point
    do {
        for (NSInteger y = 2; y < self.tiles.gridSize.height - 2; y++)
        {
            for (NSInteger x = 2; x < self.tiles.gridSize.width - 2; x++)
            {
                CGPoint tileCoordinate = CGPointMake(x, y);
                MapTileType tileType = [self.tiles tileTypeAt:tileCoordinate];
                if (tileType == MapTileTypeFloor && random_int(0, 1000) == 7) {
                    
                    // TODO: make sure this doesn't spawn by a door.
                    _exitPoint = [self convertMapCoordinateToWorldCoordinate:CGPointMake(tileCoordinate.x, tileCoordinate.y)];
                }
            }
        }
    } while (CGPointEqualToPoint(self.exitPoint, CGPointZero));
}

// When positioning the tile, the current code sets the tile’s position to the position within the internal grid and not relative to screen coordinates.
// Convert grid coordinates into screen coordinates,
- (CGPoint)convertMapCoordinateToWorldCoordinate:(CGPoint)mapCoordinate
{
    return CGPointMake(mapCoordinate.x * self.tileSize,  (self.tiles.gridSize.height - mapCoordinate.y) * self.tileSize);
}

#pragma mark - Walls
- (void)generateWalls
{
    // First loop through each tile of the grid.
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        for (NSInteger x = 0; x < self.tiles.gridSize.width; x++)
        {
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // Until it identifies a floor tile (MapTileTypeFloor)
            if ([self.tiles tileTypeAt:tileCoordinate] == MapTileTypeFloor)
            {
                for (NSInteger neighbourY = -1; neighbourY < 2; neighbourY++)
                {
                    for (NSInteger neighbourX = -1; neighbourX < 2; neighbourX++)
                    {
                        if (!(neighbourX == 0 && neighbourY == 0))
                        {
                            CGPoint coordinate = CGPointMake(x + neighbourX, y + neighbourY);
                            
                            // It then checks the surrounding tiles and marks these as walls (MapTileTypeWall)
                            if ([self.tiles tileTypeAt:coordinate] == MapTileTypeNone)
                            {
                                [self.tiles setTileType:MapTileTypeWall at:coordinate];
                            }
                        }
                    }
                }
            }
        }
    }
}

- (void)addCollisionWallAtPosition:(CGPoint)position withSize:(CGSize)size
{
    SKNode *wall = [SKNode node];
    wall.position = CGPointMake(position.x + size.width * 0.5f - 0.5f * self.tileSize,
                                position.y - size.height * 0.5f + 0.5f * self.tileSize);
    wall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:size];
    wall.physicsBody.dynamic = NO;
    wall.physicsBody.categoryBitMask = CollisionTypeWall;
    wall.physicsBody.contactTestBitMask = 0;
    wall.physicsBody.collisionBitMask = CollisionTypePlayer | CollisionTypeEnemy;
    
    [self addChild:wall];
}

- (void)generateCollisionWalls
{
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        CGFloat startPointForWall = 0;
        CGFloat wallLength = 0;
        for (NSInteger x = 0; x <= self.tiles.gridSize.width; x++)
        {
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // Iterate through each row until you find a wall tile.
            // You set a starting point (tile coordinate pair) for the collision wall and then increase the wallLength by one.
            // Then move to the next tile. If this is also a wall tile, you repeat these steps.
            if ([self.tiles tileTypeAt:tileCoordinate] == MapTileTypeWall)
            {
                if (startPointForWall == 0 && wallLength == 0)
                {
                    startPointForWall = x;
                }
                wallLength += 1;
            }
            
            // If the next tile is not a wall tile, you calculate the size of the wall in points by multiplying the tile size, and you convert the starting point into world coordinates.
            else if (wallLength > 0)
            {
                CGPoint wallOrigin = CGPointMake(startPointForWall, y);
                CGSize wallSize = CGSizeMake(wallLength * self.tileSize, self.tileSize);
                [self addCollisionWallAtPosition:[self convertMapCoordinateToWorldCoordinate:wallOrigin]
                                        withSize:wallSize];
                startPointForWall = 0;
                wallLength = 0;
            }
        }
    }
}

#pragma mark - Rooms

// This method adds a room with its top-left corner at the passed position with the passed size and returns a value representing the number of tiles created.
// If the room overlaps any existing floor tiles, then that overlap is not counted in the number returned by the method.
- (NSUInteger)generateRoomAt:(CGPoint)position withSize:(CGSize)size
{
    NSUInteger numberOfFloorsGenerated = 0;
    for (NSUInteger y = 0; y < size.height; y++)
    {
        for (NSUInteger x = 0; x < size.width; x++)
        {
            CGPoint tilePosition = CGPointMake(position.x + x, position.y + y);
            
            if ([self.tiles tileTypeAt:tilePosition] == MapTileTypeInvalid)
            {
                continue;
            }
            
            if (![self.tiles isEdgeTileAt:tilePosition])
            {
                if ([self.tiles tileTypeAt:tilePosition] == MapTileTypeNone)
                {
                    [self.tiles setTileType:MapTileTypeFloor at:tilePosition];
                    
                    numberOfFloorsGenerated++;
                }
            }
        }
    }
    return numberOfFloorsGenerated;
}

#pragma mark - Doors
- (void)generateDoors
{
    // First loop through each tile of the grid.
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        for (NSInteger x = 0; x < self.tiles.gridSize.width; x++)
        {
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // Until it identifies a floor tile (MapTileTypeFloor)
            if ([self.tiles tileTypeAt:tileCoordinate] == MapTileTypeFloor)
            {
                CGPoint coordinateAbove = CGPointMake(tileCoordinate.x, tileCoordinate.y - 1);
                CGPoint coordinateBelow = CGPointMake(tileCoordinate.x, tileCoordinate.y + 1);
                CGPoint coordinateLeft  = CGPointMake(tileCoordinate.x - 1, tileCoordinate.y);
                CGPoint coordinateRight = CGPointMake(tileCoordinate.x + 1, tileCoordinate.y);
                
                // Check for only two walls adjacent to a floor tile
                // Then check the surrounding tiles as walls (MapTileTypeWall)
                
                if ([self.tiles tileTypeAt:coordinateAbove] == MapTileTypeWall && [self.tiles tileTypeAt:coordinateBelow] == MapTileTypeWall)
                {
                    if ([self.tiles tileTypeAt:coordinateLeft] == MapTileTypeDoor || [self.tiles tileTypeAt:coordinateRight] == MapTileTypeDoor) continue;
                    if ([self.tiles tileTypeAt:coordinateLeft] == MapTileTypeWall || [self.tiles tileTypeAt:coordinateRight] == MapTileTypeWall) continue;
                    
                    [self setDoorTypeAtCoordinate:tileCoordinate];
                }
                
                else if ([self.tiles tileTypeAt:coordinateLeft] == MapTileTypeWall && [self.tiles tileTypeAt:coordinateRight] == MapTileTypeWall)
                {
                    if ([self.tiles tileTypeAt:coordinateAbove] == MapTileTypeDoor || [self.tiles tileTypeAt:coordinateBelow] == MapTileTypeDoor) continue;
                    if ([self.tiles tileTypeAt:coordinateAbove] == MapTileTypeWall || [self.tiles tileTypeAt:coordinateBelow] == MapTileTypeWall) continue;
                    
                    [self setDoorTypeAtCoordinate:tileCoordinate];
                }
            }
        }
    }
}

- (void)addCollisionDoorAtPosition:(CGPoint)position withSize:(CGSize)size
{
    SKNode *door = [SKNode node];
    
    door.position = CGPointMake(position.x + size.width * 0.5f - 0.5f * self.tileSize,
                                position.y - size.height * 0.5f + 0.5f * self.tileSize);
    door.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:size];
    door.physicsBody.dynamic = NO;
    door.physicsBody.categoryBitMask = CollisionTypeDoor;
    door.physicsBody.contactTestBitMask = CollisionTypePlayer;
    door.physicsBody.collisionBitMask = CollisionTypePlayer;
    
    [self addChild:door];
}

- (void)setDoorTypeAtCoordinate:(CGPoint)tileCoordinate
{
    [self.tiles setTileType:MapTileTypeDoor at:tileCoordinate];
    
    [self addCollisionDoorAtPosition:[self convertMapCoordinateToWorldCoordinate:tileCoordinate]
                            withSize:CGSizeMake(32.0f, 32.0f)];
}

#pragma mark - Generate doorways
- (void)generateDoorway
{
    // First loop through each tile of the grid.
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        for (NSInteger x = 0; x < self.tiles.gridSize.width; x++)
        {
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // Until it identifies an invalid tile (MapTileTypeInvalid)
            if ([self.tiles tileTypeAt:tileCoordinate] == MapTileTypeNone)
            {
                CGPoint coordinate2Above = CGPointMake(tileCoordinate.x, tileCoordinate.y - 2);
                CGPoint coordinateAbove = CGPointMake(tileCoordinate.x, tileCoordinate.y - 1);
                CGPoint coordinate2Below = CGPointMake(tileCoordinate.x, tileCoordinate.y + 2);
                CGPoint coordinateBelow = CGPointMake(tileCoordinate.x, tileCoordinate.y + 1);
                CGPoint coordinate2Left  = CGPointMake(tileCoordinate.x - 2, tileCoordinate.y);
                CGPoint coordinateLeft  = CGPointMake(tileCoordinate.x - 1, tileCoordinate.y);
                CGPoint coordinate2Right = CGPointMake(tileCoordinate.x + 2, tileCoordinate.y);
                CGPoint coordinateRight = CGPointMake(tileCoordinate.x + 1, tileCoordinate.y);
                
                // Check for two floor tiles adjacent to an invalid tile
            
                if ([self.tiles tileTypeAt:coordinateAbove] == MapTileTypeFloor && [self.tiles tileTypeAt:coordinateBelow] == MapTileTypeFloor)
                {
                    if ([self.tiles tileTypeAt:coordinateLeft] == MapTileTypeFloor || [self.tiles tileTypeAt:coordinateRight] == MapTileTypeFloor) continue;
                    if ([self.tiles tileTypeAt:coordinate2Left] == MapTileTypeFloor || [self.tiles tileTypeAt:coordinate2Right] == MapTileTypeFloor) continue;
                    
                    if (random_int(0, 4) == 1) {
                        [self.tiles setTileType:MapTileTypeFloor at:tileCoordinate];
                    }
                }
                
                else if ([self.tiles tileTypeAt:coordinateLeft] == MapTileTypeFloor && [self.tiles tileTypeAt:coordinateRight] == MapTileTypeFloor)
                {
                    if ([self.tiles tileTypeAt:coordinateAbove] == MapTileTypeFloor || [self.tiles tileTypeAt:coordinateBelow] == MapTileTypeFloor) continue;
                    if ([self.tiles tileTypeAt:coordinate2Above] == MapTileTypeFloor || [self.tiles tileTypeAt:coordinate2Below] == MapTileTypeFloor) continue;
                    
                    if (random_int(0, 4) == 1) {
                        [self.tiles setTileType:MapTileTypeFloor at:tileCoordinate];
                    }
                }
            }
        }
    }
}

#pragma mark - Generate enemies
- (void)generateEnemies
{
    self.enemies = [[NSMutableArray alloc] initWithArray:@[]];
    
    // First loop through each tile of the grid.
    
    for (NSInteger y = 0; y < self.tiles.gridSize.height; y++)
    {
        for (NSInteger x = 0; x < self.tiles.gridSize.width; x++)
        {
            CGPoint coordinate = CGPointMake(x, y);
            
            // Until it identifies a floor tile (MapTileTypeFloor)
            if ([self.tiles tileTypeAt:coordinate] == MapTileTypeFloor)
            {
                // Random chance to create an enemy.
                if (random_int(0, 5) == 1) {
                    [self.enemies addObject:[NSValue valueWithCGPoint:[self convertMapCoordinateToWorldCoordinate:coordinate]]];
                }
            }
        }
    }
}

@end
