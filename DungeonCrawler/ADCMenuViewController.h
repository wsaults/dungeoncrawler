//
//  ADCMenuViewController.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/22/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

@interface ADCMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, GKGameCenterControllerDelegate, UIAlertViewDelegate>

@end
