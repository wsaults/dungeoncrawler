//
//  ADCTextureManager.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/3/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCTextureManager.h"

@implementation ADCTextureManager

+ (instancetype)sharedManager{
    static ADCTextureManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        [self prepAtlases];
    }
    return self;
}


- (void)prepAtlases
{
    self.spriteAtlas = [SKTextureAtlas atlasNamed:@"sprites"];
    self.itemsAtlas = [SKTextureAtlas atlasNamed:@"items"];
    self.attacksAtlas = [SKTextureAtlas atlasNamed:@"attacks"];
}

@end
