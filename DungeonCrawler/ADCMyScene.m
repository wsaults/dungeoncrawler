// //
//  ADCMyScene.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCMyScene.h"
#import "ADCMap.h"
#import "DPad.h"
#import "ADCMapTiles.h"
#import "ADCPlayer.h"
#import "ADCEnemy.h"
#import "ADCLootDropManager.h"
#import "ADCUtilities.h"
#import "ADCTextureManager.h"
#import "ADCItem.h"
#import "ADCGold.h"
#import "ADCModelManager.h"
#import "User.h"
#import "Player.h"
#import "Weapon.h"
#import "ADCPotion.h"
#import <AVFoundation/AVFoundation.h>

@interface ADCMyScene() <SKPhysicsContactDelegate>

@property (nonatomic, assign) NSTimeInterval lastUpdateTimeInterval;
@property (nonatomic, strong) SKNode *world;
@property (nonatomic, assign) NSUInteger worldLevel;
@property (nonatomic, strong) ADCMap *map;
@property (nonatomic, strong) SKSpriteNode *exit;
@property (nonatomic, strong) SKNode *hud;
@property (nonatomic, strong) DPad *movementControl;
@property (nonatomic, strong) DPad *attackControl;

@property (nonatomic, strong) ADCPlayer *player;
@property (nonatomic, strong) NSMutableArray *enemies;

@property (nonatomic, getter = isExitingLevel) BOOL exitingLevel;
@property (nonatomic, getter = isUnspentPointNoteRunning) BOOL unspentPointNoteRunning;

@property (nonatomic, strong) SKSpriteNode *lifeBar;
@property (nonatomic, strong) SKSpriteNode *expBar;
@property (nonatomic, strong) SKSpriteNode *underLifeBar;
@property (nonatomic, strong) SKSpriteNode *underExpBar;
@property (nonatomic, strong) SKLabelNode *lifeLabel;
@property (nonatomic, strong) SKLabelNode *expLabel;
@property (nonatomic, assign) CGFloat requiredExpToLevelUp;
@property (nonatomic, strong) SKLabelNode *goldLabel;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSNumber *selectedPlayerId;

#pragma mark - Audio
@property (nonatomic, strong) SKAction *coinPickupSound;
@property (nonatomic, strong) SKAction *potionPickupSound;
@property (nonatomic, strong) SKAction *potionDropSound;
@property (nonatomic, strong) SKAction *coinDropSound;
@property (nonatomic, strong) SKAction *itemDropSound;
@property (nonatomic, strong) SKAction *flameSound;

// Define the completion block
typedef void(^resetLevelCompletion)(BOOL);

@end

@implementation ADCMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [SKColor blackColor];
        
        // Initialize physics
        self.physicsWorld.gravity = CGVectorMake(0, 0); // The gravity is zero because we don't want things falling off the screen.
        self.physicsWorld.contactDelegate = self;
        
        // TODO: this will be set to 1 for new games, incremented when going down a level, selected when playing a previously played character.
        self.worldLevel = 1;
        
        // Setup coredata entities
        NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
        self.user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
        
        self.selectedPlayerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerId"];
        
        self.savedPlayer = [[ADCModelManager sharedManager] playerWithId:self.selectedPlayerId.integerValue forUser:self.user];
        self.worldLevel = self.savedPlayer.farthestFloorReached.integerValue;
        
        [self createLevel];
        
        [self addObservers];
        
        self.coinPickupSound = [SKAction playSoundFileNamed:@"coin_pickup.wav" waitForCompletion:NO];
        self.potionPickupSound = [SKAction playSoundFileNamed:@"health_potion.wav" waitForCompletion:NO];
        self.potionDropSound = [SKAction playSoundFileNamed:@"potion_drop.wav" waitForCompletion:NO];
        self.coinDropSound = [SKAction playSoundFileNamed:@"coin_drop.wav" waitForCompletion:NO];
        self.itemDropSound = [SKAction playSoundFileNamed:@"item_drop.wav" waitForCompletion:NO];
        self.flameSound = [SKAction playSoundFileNamed:@"flame.wav" waitForCompletion:NO];
    }
    
    return self;
}

-(void)resetLevel:(resetLevelCompletion)compblock
{
    NSLog(@"remove all...");
    
    // Just to be sure that the dpads are gone.
    [self.movementControl removeFromParent];
    [self.attackControl removeFromParent];
    self.movementControl = nil;
    self.attackControl = nil;
    
    self.enemies = nil;
    
    [self removeAllActions];
    [self removeAllChildren];
    
    compblock(YES);
}

- (void)createLevel
{
    [self resetLevel:^(BOOL finished) {
        if(finished){
            NSLog(@"remove all complete");
            
            // Add a node for the world - this is where sprites and tiles are added
            self.world = [SKNode node];
            
            // Create a node for the HUD - this is where the DPad to control the player sprite will be added
            self.hud = [SKNode node];
            
            [self addMap];
            [self addEnemies];
            [self addPlayer];
            [self addHudElements];
            
            // Add the world and hud nodes to the scene
            [self addChild:self.world];
            [self addChild:self.hud];
            
            self.exitingLevel = NO;
            self.unspentPointNoteRunning = NO;
        }
    }];
}

- (void)addMap
{
    // Create a new map
    self.map = [[ADCMap alloc] initWithGridSize:CGSizeMake(48, 48)];
    [self.map setSpriteAtlas:[[ADCTextureManager sharedManager] spriteAtlas]];
    [self.map generateLevel:self.worldLevel];
    [self.world addChild:self.map];
    
    NSMutableString *tileMapDescription = [NSMutableString stringWithFormat:@""];
    for (int y = ((int)self.map.gridSize.height - 1); y >= 0; y--)
    {
        for (NSInteger x = 0; x < (NSInteger)self.map.gridSize.width; x++ )
        {
            [tileMapDescription appendString:[NSString stringWithFormat:@"%i", (int)[self.map.tiles tileTypeAt:CGPointMake(x, y)]]];
        }
        [tileMapDescription appendString:@"\n"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setMap" object:tileMapDescription];
    
    // Create the exit
    self.exit = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"exit"]];
    self.exit.position = self.map.exitPoint;
    self.exit.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.exit.texture.size.width - 16, self.exit.texture.size.height - 16)];
    self.exit.physicsBody.categoryBitMask = CollisionTypeExit;
    self.exit.physicsBody.contactTestBitMask = CollisionTypePlayer;
    [self.exit.physicsBody setDynamic:NO];
    [self.map addChild:self.exit];
}

- (void)addEnemies
{
    // Create enemies
    self.enemies = nil;
    self.enemies = [[NSMutableArray alloc] initWithCapacity:self.map.enemies.count];
    for (NSValue *enemyLocation in self.map.enemies) {
        ADCEnemy *enemy = [[ADCEnemy alloc] initWithPosition:[enemyLocation CGPointValue] withType:(int)random_int(0, 4)];
        
        NSInteger floorLevel = self.worldLevel; // Adjust this number based on floor lv
        
        // Stats
        // Adjust these numbers based on floor lv
        CGFloat x = pow(2, floorLevel);
        
        [enemy setVitality:x];
        [enemy setDamage:x];
        [enemy setAttackRate:2];
        [enemy setName:@"enemy"];
        
        CGFloat min = pow(2, floorLevel);
        CGFloat max = pow(6, floorLevel);
        [enemy setExpValue:boris_random(min / 5, max / 5)];
        
        [self.world addChild:enemy];
        [self.enemies addObject:enemy];
    }
}

- (void)addPlayer
{
    // Create a player node
    self.player = [ADCPlayer spriteNodeWithTexture:[[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"player"]];
    self.player.anchorPoint = CGPointMake(.5, .5);
    self.player.position = self.map.spawnPoint;
    self.player.physicsBody.allowsRotation = NO;
    self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.texture.size];
    self.player.physicsBody.categoryBitMask = CollisionTypePlayer;
    self.player.physicsBody.contactTestBitMask = CollisionTypeExit | CollisionTypeDoor | CollisionTypeEnemy;
    self.player.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypeDoor | CollisionTypeEnemy;
    
    // Stats
    [self.player setVitality:self.savedPlayer.vitality.floatValue];
    [self.player setStrength:self.savedPlayer.vitality.floatValue];
    [self.player setDefense:self.savedPlayer.defense.floatValue];
    [self.player setDamage:self.savedPlayer.vitality.floatValue + self.savedPlayer.weapon.strength.floatValue];
    [self.player setAttackRate:self.savedPlayer.attack_rate.floatValue];
    [self.player setMovementSpeed:self.savedPlayer.movement_speed.floatValue];
    
    [self.world addChild:self.player];
}

- (void)addHudElements
{
    // Create the player controls
    self.movementControl = [[DPad alloc] initWithRect:CGRectMake(0, 0, 64.0f, 64.0f) andType:MOVE];
    self.movementControl.position = CGPointMake(10, 200);
    self.movementControl.numberOfDirections = 24;
    self.movementControl.deadRadius = 8.0f;
    
    self.attackControl = [[DPad alloc] initWithRect:CGRectMake(0, 0, 64.0f, 64.0f) andType:ATTACK];
    self.attackControl.position = CGPointMake(245, 200);
    self.attackControl.numberOfDirections = 8;
    self.attackControl.deadRadius = 8.0f;
    
    // Life UI
    self.lifeBar = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(150 * self.player.vitality/100, 5)];
    self.lifeBar.anchorPoint = CGPointMake(0, .5f);
    self.underLifeBar = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(150 * self.player.vitality/100, 5)];
    self.underLifeBar.anchorPoint = CGPointMake(0, .5f);
    self.underLifeBar.alpha = .50f;
    
    self.lifeLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
    [self.lifeLabel setFontSize:8.0f];
    [self.lifeLabel setFontColor:[SKColor whiteColor]];
    
    // Exp UI
    self.expBar = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(150, 5)];
    self.expBar.anchorPoint = CGPointMake(0, .5f);
    
    self.underExpBar = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(150, 5)];
    self.underExpBar.anchorPoint = CGPointMake(0, .5f);
    self.underExpBar.alpha = .50f;
    
    self.expLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
    [self.expLabel setFontSize:8.0f];
    [self.expLabel setFontColor:[SKColor blackColor]];
    
    NSInteger lv = self.savedPlayer.level.integerValue; // Use the required exp to determine expBar height.
    self.requiredExpToLevelUp = lv * (50 * lv);
    
    self.goldLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %d", self.user.goldCount.intValue]];
    [self.goldLabel setFontSize:12.0f];
    
    [self.hud addChild:self.movementControl];
    [self.hud addChild:self.attackControl];
    [self.world addChild:self.underLifeBar];
    [self.world addChild:self.lifeBar]; // this doesn't appear when added to the hud node for some odd reason.
    [self.world addChild:self.lifeLabel];
    [self.world addChild:self.underExpBar];
    [self.world addChild:self.expBar];
    [self.world addChild:self.expLabel];
    [self.world addChild:self.goldLabel];
}

#pragma mark - Observers
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserverForName:@"ItemDrop" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSLog(@"item dropped");
        
        NSValue *position = note.object;
        
        ADCItem *item = [[ADCLootDropManager sharedManager] item];
        item.position = [position CGPointValue];
        item.anchorPoint = CGPointMake(.5, .5);
        
        [self.map addChild:item];
        
        [self.scene runAction:self.itemDropSound];
        [self performLootDropAnimationOnNode:item shouldRotate:YES];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"GoldDrop" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSLog(@"gold dropped");
        
        NSValue *position = note.object;
        CGPoint location = [position CGPointValue];
        
        ADCGold *gold = [[ADCLootDropManager sharedManager] gold];
        gold.position = CGPointMake(location.x + random_int(10, 30), location.y + random_int(10, 30));
        gold.anchorPoint = CGPointMake(.5, .5);
        
        [self.map addChild:gold];
        
        [self.scene runAction:self.coinDropSound];
        [self performLootDropAnimationOnNode:gold shouldRotate:NO];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"PotionDrop" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSLog(@"potion dropped");
        
        NSValue *position = note.object;
        CGPoint location = [position CGPointValue];
        
        ADCPotion *potion = [[ADCLootDropManager sharedManager] potion];
        potion.position = CGPointMake(location.x + random_int(30, 50), location.y + random_int(30, 50));
        potion.anchorPoint = CGPointMake(.5, .5);
        
        [self.map addChild:potion];
        
        [self.scene runAction:self.potionDropSound];
        [self performLootDropAnimationOnNode:potion shouldRotate:NO];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"EnemyDied" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSLog(@"removing enemy from array");
        
        // TODO: move this exp business elsewhere.
        
        ADCEnemy *enemy = (ADCEnemy *)note.object;
        
        CGFloat currentExp = self.savedPlayer.experiance.floatValue;
        NSNumber *newExp = [NSNumber numberWithFloat:currentExp + enemy.expValue];
        [self.savedPlayer setExperiance:newExp];
        NSLog(@"exp: %f", self.savedPlayer.experiance.floatValue);
        
        // Check for lv up when exp is gained.
        // Requried exp to level up = x^2 * (50 * x) | x = current level.
        NSInteger lv = self.savedPlayer.level.integerValue;
        self.requiredExpToLevelUp = lv * (50 * lv);
        
        
        if (self.savedPlayer.experiance.floatValue >= self.requiredExpToLevelUp) {
        
#pragma mark - Player level up.
            [[ADCModelManager sharedManager] levelUpPlayer:self.savedPlayer];
            
            // Refill the player's health when they level up.
            [self.player setVitality:self.savedPlayer.vitality.floatValue];
            
            // Show lv up particle emitter.
            NSString *particlePath = [[NSBundle mainBundle] pathForResource:@"LevelUp" ofType:@"sks"];
            SKEmitterNode *levelUpParticle = [NSKeyedUnarchiver unarchiveObjectWithFile:particlePath];
            levelUpParticle.particlePosition = self.player.position;
            levelUpParticle.particleBirthRate = 10;
            levelUpParticle.numParticlesToEmit = 10;
            [self.world addChild:levelUpParticle];
            
            // Make gold pickup label that fades out above the player's head.
            SKLabelNode *levelUpLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
            [levelUpLabel setText:[NSString stringWithFormat:@"LEVEL %d", self.savedPlayer.level.intValue]];
            [levelUpLabel setPosition:CGPointMake(self.player.position.x, self.player.position.y + 20)];
//            [levelUpLabel setFontSize:12.0f];
            [self.world addChild:levelUpLabel];
            
            SKAction *fade = [SKAction fadeAlphaTo:0 duration:1];
            SKAction *rise = [SKAction moveByX:0 y:20 duration:1];
            SKAction *group = [SKAction group:@[fade, rise]];
            SKAction *block = [SKAction runBlock:^{
                [levelUpLabel removeFromParent];
            }];
            SKAction *sequence = [SKAction sequence:@[group, block]];
            
            [levelUpLabel runAction:sequence];
        }
        
        [[ADCModelManager sharedManager] saveContext];
        
        // Enemy death animation
        
        SKSpriteNode *slimeNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"slime"]];
        slimeNode.anchorPoint = CGPointMake(.5, .5);
        slimeNode.position = enemy.position;
        [self.map addChild:slimeNode];
        
        [self.enemies removeObject:enemy];
        
//        SKAction *slimeBlock = [SKAction runBlock:^{ [self performLootDropAnimationOnNode:slimeNode shouldRotate:NO]; }];
        
        SKAction *wait = [SKAction waitForDuration:2];
        SKAction *fade = [SKAction fadeOutWithDuration:2];
        
        SKAction *fadeBlock = [SKAction runBlock:^{
            [slimeNode runAction:fade];
        }];
        
        SKAction *removeBlock = [SKAction runBlock:^{
            [slimeNode removeFromParent];
        }];
        
        SKAction *sequence = [SKAction sequence:@[wait, fadeBlock, wait, removeBlock]];
        [self runAction:sequence];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"PlayerDied" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSLog(@"player died");
        [self.savedPlayer setIs_alive:[NSNumber numberWithBool:NO]];
        [[ADCModelManager sharedManager] saveContext];
        
        SKSpriteNode *bonesNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"player_bones"]];
        bonesNode.anchorPoint = CGPointMake(.5, .5);
        bonesNode.position = self.player.position;
        [self.map addChild:bonesNode];
        
        SKSpriteNode *weaponNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] itemsAtlas] textureNamed:@"weapon"]];
        weaponNode.anchorPoint = CGPointMake(.5, .5);
        weaponNode.position = CGPointMake(self.player.position.x + 10, self.player.position.y + 10);
        weaponNode.alpha = 0;
        [self.map addChild:weaponNode];
        
        SKSpriteNode *offHandNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] itemsAtlas] textureNamed:@"off_hand"]];
        offHandNode.anchorPoint = CGPointMake(.5, .5);
        offHandNode.position = CGPointMake(self.player.position.x - 10, self.player.position.y - 10);
        offHandNode.alpha = 0;
        [self.map addChild:offHandNode];
        
        SKSpriteNode *headGearNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] itemsAtlas] textureNamed:@"head_gear"]];
        headGearNode.anchorPoint = CGPointMake(.5, .5);
        headGearNode.position = CGPointMake(self.player.position.x + 10, self.player.position.y - 10);
        headGearNode.alpha = 0;
        [self.map addChild:headGearNode];
        
        SKSpriteNode *skullNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"player_skull"]];
        skullNode.anchorPoint = CGPointMake(.5, .5);
        skullNode.size = CGSizeMake(skullNode.size.width/2, skullNode.size.height/2);
        skullNode.position = self.player.position;
        skullNode.alpha = 0;
        [self.map addChild:skullNode];
        
        [self.player setPhysicsBody:nil];
        [self.player removeFromParent];
        
        SKAction *wait = [SKAction waitForDuration:.2];
        
        SKAction *bonesBlock = [SKAction runBlock:^{ [self performLootDropAnimationOnNode:bonesNode shouldRotate:NO]; }];
        SKAction *skullBlock = [SKAction runBlock:^{
            skullNode.alpha = 1;
            [self performLootDropAnimationOnNode:skullNode shouldRotate:YES];
        }];
        
        SKAction *weaponBlock = [SKAction runBlock:^{
            weaponNode.alpha = 1;
            [self performLootDropAnimationOnNode:weaponNode shouldRotate:YES];
            [self.scene runAction:self.itemDropSound];
        }];
        
        SKAction *offHandBlock = [SKAction runBlock:^{
            offHandNode.alpha = 1;
            [self performLootDropAnimationOnNode:offHandNode shouldRotate:YES];
        }];
        
        SKAction *headGearBlock = [SKAction runBlock:^{
            headGearNode.alpha = 1;
            [self performLootDropAnimationOnNode:offHandNode shouldRotate:YES];
        }];
        
        SKAction *pauseView = [SKAction runBlock:^{
            // Show game over screen
            [[NSNotificationCenter defaultCenter] postNotificationName:@"togglePauseMenu" object:nil];
        }];
        
        SKAction *sequence = [SKAction sequence:@[bonesBlock, wait, weaponBlock, wait, offHandBlock, wait, headGearBlock, wait, skullBlock, wait, pauseView]];
        [self runAction:sequence];
        
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"unpause" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self setPaused:NO];
        [self.world setPaused:NO];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"pause" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self setPaused:YES];
        [self.world setPaused:YES];
    }];
    
    // Skills
    [[NSNotificationCenter defaultCenter] addObserverForName:@"flameSkill" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        
        // Animate flames at player position
        
        SKTexture *flameTexture = [[[ADCTextureManager sharedManager] spriteAtlas] textureNamed:@"flame"];
        
        SKSpriteNode *flameNode1 = [SKSpriteNode spriteNodeWithTexture:flameTexture];
        flameNode1.size = CGSizeMake(flameNode1.size.width / 2, flameNode1.size.height / 2);
        flameNode1.position = self.player.position;
        flameNode1.physicsBody.allowsRotation = NO;
        flameNode1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flameTexture.size];
        flameNode1.physicsBody.categoryBitMask = CollisionTypeSpell;
        flameNode1.physicsBody.contactTestBitMask =  CollisionTypeEnemy;
        flameNode1.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypeEnemy;
        [self.map addChild:flameNode1];
        
        SKSpriteNode *flameNode2 = [SKSpriteNode spriteNodeWithTexture:flameTexture];
        flameNode2.size = CGSizeMake(flameNode2.size.width / 2, flameNode2.size.height / 2);
        flameNode2.position = self.player.position;
        flameNode2.physicsBody.allowsRotation = NO;
        flameNode2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flameTexture.size];
        flameNode2.physicsBody.categoryBitMask = CollisionTypeSpell;
        flameNode2.physicsBody.contactTestBitMask =  CollisionTypeEnemy;
        flameNode2.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypeEnemy;
        [self.map addChild:flameNode2];
        
        SKSpriteNode *flameNode3 = [SKSpriteNode spriteNodeWithTexture:flameTexture];
        flameNode3.size = CGSizeMake(flameNode3.size.width / 2, flameNode3.size.height / 2);
        flameNode3.position = self.player.position;
        flameNode3.physicsBody.allowsRotation = NO;
        flameNode3.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flameTexture.size];
        flameNode3.physicsBody.categoryBitMask = CollisionTypeSpell;
        flameNode3.physicsBody.contactTestBitMask =  CollisionTypeEnemy;
        flameNode3.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypeEnemy;
        [self.map addChild:flameNode3];
        
        SKSpriteNode *flameNode4 = [SKSpriteNode spriteNodeWithTexture:flameTexture];
        flameNode4.size = CGSizeMake(flameNode4.size.width / 2, flameNode4.size.height / 2);
        flameNode4.position = self.player.position;
        flameNode4.physicsBody.allowsRotation = NO;
        flameNode4.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flameTexture.size];
        flameNode4.physicsBody.categoryBitMask = CollisionTypeSpell;
        flameNode4.physicsBody.contactTestBitMask =  CollisionTypeEnemy;
        flameNode4.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypeEnemy;
        [self.map addChild:flameNode4];
        
        [self.scene runAction:self.flameSound];
        
        SKAction *move1 = [SKAction moveByX:0 y:40 duration:.4];
        SKAction *move2 = [SKAction moveByX:-40 y:0 duration:.4];
        SKAction *move3 = [SKAction moveByX:40 y:0 duration:.4];
        SKAction *move4 = [SKAction moveByX:0 y:-40 duration:.4];
        
        SKAction *fade = [SKAction fadeOutWithDuration:.3];
        
        SKAction *block1 = [SKAction runBlock:^{ [flameNode1 removeFromParent]; }];
        SKAction *block2 = [SKAction runBlock:^{ [flameNode2 removeFromParent]; }];
        SKAction *block3 = [SKAction runBlock:^{ [flameNode3 removeFromParent]; }];
        SKAction *block4 = [SKAction runBlock:^{ [flameNode4 removeFromParent]; }];
        
        SKAction *seqence1 = [SKAction sequence:@[move1, fade, block1]];
        SKAction *seqence2 = [SKAction sequence:@[move2, fade, block2]];
        SKAction *seqence3 = [SKAction sequence:@[move3, fade, block3]];
        SKAction *seqence4 = [SKAction sequence:@[move4, fade, block4]];
        
        [flameNode1 runAction:seqence1];
        [flameNode2 runAction:seqence2];
        [flameNode3 runAction:seqence3];
        [flameNode4 runAction:seqence4];
    }];
}

- (void)performLootDropAnimationOnNode:(SKNode *)node shouldRotate:(BOOL)shouldRotate
{
    // Animate drop.
    SKAction *tossUp = [SKAction moveByX:0 y:30 duration:.2];
    SKAction *fallDown = [SKAction moveByX:0 y:-30 duration:.15];
    SKAction *rotation = [SKAction rotateByAngle:boris_random(45, 360) duration:.35];
    
    SKAction *sequence = [SKAction sequence:@[tossUp, fallDown]];
    
    NSArray *groupArray = (shouldRotate ? @[sequence, rotation] : @[sequence]);
    SKAction *group = [SKAction group:groupArray];
    
    [node runAction:group completion:^{
    }];
}

#pragma mark - Update
-(void)update:(CFTimeInterval)currentTime {
    if (self.isPaused) {
        return;
    }
    /* Called before each frame is rendered */
    
    // Calculate the time since last update
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    
    self.lastUpdateTimeInterval = currentTime;
    
    if (timeSinceLast > 1)
    {
        timeSinceLast = 1.0f / 60.0f;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    if (self.player.isDying) return;
    
    // Poll the movement control
    CGPoint playerVelocity = self.isExitingLevel ? CGPointZero : self.movementControl.velocity;
    
    // Update player sprite position and orientation based on movement input
    CGPoint playerPosition = CGPointMake(self.player.position.x + playerVelocity.x * timeSinceLast * self.player.movementSpeed,
                                         self.player.position.y + playerVelocity.y * timeSinceLast * self.player.movementSpeed);
    [self.player updatePosition:playerPosition];
    
    CGPoint attackPoint = CGPointZero;
    
    // Attack based on the stick position
    if (!CGPointEqualToPoint(self.attackControl.stickPosition, CGPointZero)) {

//        NSLog(@"Attack degrees: %f", self.attackControl.degrees);
        
        CGFloat x = self.player.position.x;
        CGFloat y = self.player.position.y;
        
        int change = 30;
        
        // TODO: consider an algorithm to handle this
        switch ((int)self.attackControl.degrees) {
            case 45:
                attackPoint = CGPointMake(x + change, y + change);
                break;
                
            case 90:
                attackPoint = CGPointMake(x, y + change);
                break;
                
            case 135:
                attackPoint = CGPointMake(x - change, y + change);
                break;
                
            case 180:
                attackPoint = CGPointMake(x - change, y);
                break;
                
            case 225:
                attackPoint = CGPointMake(x - change, y - change);
                break;
                
            case 270:
                attackPoint = CGPointMake(x, y - change);
                break;
                
            case 315:
                attackPoint = CGPointMake(x + change, y - change);
                break;
                
            default: // Either 0 or 360
                attackPoint = CGPointMake(x + change, y);
                break;
        }
        
//        NSLog(@"player point: %@", NSStringFromCGPoint(self.player.position));
//        NSLog(@"Attack point: %@", NSStringFromCGPoint(attackPoint));
    }
 
    // Reset the player zRotation to keep him straight
    self.player.zRotation = 0;
    
//    NSLog(@"player position %@", NSStringFromCGPoint(self.player.position));
    
    NSArray *tmpEnemies = [NSArray arrayWithArray:self.enemies];
    
    // Update enemies
    for (ADCEnemy *enemy in tmpEnemies)
    {
        // Look for an enemy within a limited distance of the player and enable physics bodies (helps to keep the memory usage down).
        if (DistanceBetweenPoints(self.player.position, enemy.position) < 100) {
            
            // Reset enemy zRotations to keep them straight
            enemy.zRotation = 0;
            
            enemy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:enemy.texture.size];
            enemy.physicsBody.categoryBitMask = CollisionTypeEnemy;
            enemy.physicsBody.contactTestBitMask = CollisionTypePlayer | CollisionTypeSpell;
            enemy.physicsBody.collisionBitMask = CollisionTypePlayer | CollisionTypeEnemy | CollisionTypeWall;
            
            [enemy moveTowards:self.player.position withTimeInterval:timeSinceLast];
            
            // If the distance between the enemy and the player is less than -some number- then attack!
            if (DistanceBetweenPoints(self.player.position, enemy.position) < 40) {
                [enemy attackCharacter:self.player];
            }
            
            if ([enemy containsPoint:attackPoint]) {
                [self.player attackCharacter:enemy];
            }
            
        } else {
            enemy.physicsBody = nil;
        }
    }
    
    [self lootNodes];
    
    [self updateHudElements];
    
    // Move "camera" so the player is in the middle of the screen
    self.world.position = CGPointMake(-self.player.position.x + CGRectGetMidX(self.frame),
                                      -self.player.position.y + CGRectGetMidY(self.frame));
}

- (void)lootNodes
{
    [self.map enumerateChildNodesWithName:@"item" usingBlock:^(SKNode *node, BOOL *stop) {
        ADCItem *item = (ADCItem *)node;
        
        // If the distance between the item and the player is less than -some number- then let the player interact with the item!
        if (DistanceBetweenPoints(self.player.position, item.position) < 40) {
            if (!item.canBePickedUp) [item setCanBePickedUp:YES];
        } else {
            if (item.canBePickedUp) [item setCanBePickedUp:NO];
        }
    }];
    
    [self.map enumerateChildNodesWithName:@"gold" usingBlock:^(SKNode *node, BOOL *stop) {
        // If the distance between the gold and the player is less than -some number- then pick-up the gold!
        if (DistanceBetweenPoints(self.player.position, node.position) < 20) {
            ADCGold *gold = (ADCGold *)node;
            
            [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue + gold.quantity]];
            [[ADCModelManager sharedManager] saveContext];
            
            // Make gold pickup label that fades out above the player's head.
            SKLabelNode *goldPickupLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
            [goldPickupLabel setText:[NSString stringWithFormat:@"+ %d", (int)gold.quantity]];
            [goldPickupLabel setPosition:CGPointMake(self.player.position.x, self.player.position.y + 20)];
            [goldPickupLabel setFontSize:12.0f];
            [self.world addChild:goldPickupLabel];
            
            SKAction *fade = [SKAction fadeAlphaTo:0 duration:1];
            SKAction *rise = [SKAction moveByX:0 y:20 duration:1];
            SKAction *group = [SKAction group:@[fade, rise]];
            SKAction *block = [SKAction runBlock:^{
                [goldPickupLabel removeFromParent];
            }];
            SKAction *sequence = [SKAction sequence:@[group, block]];
            
            [goldPickupLabel runAction:sequence];
            
            // TODO: Gold pickup animation
            
            [self.scene runAction:self.coinPickupSound];
            
            [node removeFromParent];
        }
    }];
    
    [self.map enumerateChildNodesWithName:@"potion" usingBlock:^(SKNode *node, BOOL *stop) {
        // If the distance between the gold and the player is less than -some number- then pick-up the gold!
        if (DistanceBetweenPoints(self.player.position, node.position) < 20) {
            ADCPotion *potion = (ADCPotion *)node;
            
            if (self.player.vitality + potion.healthPoints <= self.savedPlayer.vitality.floatValue) {
                [self.player setVitality:self.player.vitality + potion.healthPoints];
            } else {
                [self.player setVitality:self.savedPlayer.vitality.floatValue];
            }

            [[ADCModelManager sharedManager] saveContext];
            
            // Make pickup label that fades out above the player's head.
            SKLabelNode *pickupLabel = [SKLabelNode labelNodeWithFontNamed:@"Copperplate"];
            [pickupLabel setText:[NSString stringWithFormat:@"+ %d HP", (int)potion.healthPoints]];
            [pickupLabel setPosition:CGPointMake(self.player.position.x, self.player.position.y + 20)];
            [pickupLabel setFontSize:12.0f];
            [self.world addChild:pickupLabel];
            
            SKAction *fade = [SKAction fadeAlphaTo:0 duration:1];
            SKAction *rise = [SKAction moveByX:0 y:20 duration:1];
            SKAction *group = [SKAction group:@[fade, rise]];
            SKAction *block = [SKAction runBlock:^{
                [pickupLabel removeFromParent];
            }];
            SKAction *sequence = [SKAction sequence:@[group, block]];
            
            [pickupLabel runAction:sequence];
            
            // TODO: Potion pickup animation
            
            [self.scene runAction:self.potionPickupSound];
            
            [node removeFromParent];
        }
    }];
}

- (void)updateHudElements
{
    int maxBarWidth = 100;
    
    // Update player hud UI
    CGFloat vit = self.player.vitality;
    CGFloat healthPercent = vit > 0 ? (vit/maxBarWidth) * 100 : 0;
    
    self.lifeBar.size = CGSizeMake(healthPercent, 10);
    self.lifeBar.position = CGPointMake(self.player.position.x - 150, self.player.position.y + 80);
    
    self.underLifeBar.size = CGSizeMake(maxBarWidth, 10);
    self.underLifeBar.position = CGPointMake(self.player.position.x - 150, self.player.position.y + 80);
    
    [self.lifeLabel setText:[NSString stringWithFormat:@"Health: %.00f/%.00f", self.player.vitality, self.savedPlayer.vitality.floatValue]];
    [self.lifeLabel setPosition:CGPointMake(self.lifeBar.position.x + 50, self.lifeBar.position.y - 3)];
    
    // Update player exp UI
    
    CGFloat exp = self.savedPlayer.experiance.floatValue;
    NSInteger lv = self.savedPlayer.level.integerValue;
    
    CGFloat max = self.requiredExpToLevelUp;
    
    // The required exp for the previous level.
    CGFloat base = (lv - 1) * (50 * (lv - 1));
    
    // Subract the current amount of exp
    exp -= base;
    
    CGFloat expPercent = exp > 0 ? (exp/maxBarWidth) * 100 : 0;
    
    expPercent = 100*(exp)/(max - base);
    
    self.expBar.size = CGSizeMake((int)expPercent, 5);
    self.expBar.position = CGPointMake(self.lifeBar.position.x, self.lifeBar.position.y - 12);
    
    self.underExpBar.size = CGSizeMake(maxBarWidth, 5);
    self.underExpBar.position = CGPointMake(self.lifeBar.position.x, self.lifeBar.position.y - 12);
    
    [self.expLabel setText:[NSString stringWithFormat:@"Exp: %.00f/%.00f", self.savedPlayer.experiance.floatValue, self.requiredExpToLevelUp]];
    [self.expLabel setPosition:CGPointMake(self.expBar.position.x + 50, self.expBar.position.y - 3)];
    
    if (self.savedPlayer.spendable_points.intValue > 0) {
        [self unspentLevelPointsAnimation];
    }
    
    [self.goldLabel setPosition:CGPointMake(self.player.position.x, self.lifeBar.position.y - 5)];
    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %d", self.user.goldCount.intValue]];
}

- (void)unspentLevelPointsAnimation
{
//    if (!self.isUnspentPointNoteRunning) {
//        self.unspentPointNoteRunning = YES;
//        
//        // TODO: make this better
//        // Show pulse animation indicating unspent lv point.
//        SKSpriteNode *pulseNode = [SKSpriteNode spriteNodeWithColor:[SKColor blueColor] size:CGSizeMake(20, 20)];
//        [pulseNode setPosition:CGPointMake(self.player.position.x + 140, self.player.position.y + 75)];
//        [self.world addChild:pulseNode];
//        
//        SKAction *fadeIn = [SKAction fadeAlphaTo:.5f duration:.7f];
//        SKAction *fadeOut = [SKAction fadeAlphaTo:0 duration:.7f];
//        
//        SKAction *block = [SKAction runBlock:^{
//            [pulseNode removeFromParent];
//            self.unspentPointNoteRunning = NO;
//        }];
//        
//        SKAction *sequence = [SKAction sequence:@[fadeIn, fadeOut, block]];
//        [pulseNode runAction:sequence];
//    }
}

- (void)exitLevel
{
    // Move player down a level.
    if (!self.isExitingLevel) {
        self.exitingLevel = YES;
        self.worldLevel++;
        [self.savedPlayer setFarthestFloorReached:[NSNumber numberWithInteger:self.worldLevel]];
        [[ADCModelManager sharedManager] saveContext];
        [self createLevel];
    }
}

- (void)openDoor
{
    // TODO: change door to open state
    // TODO: play door open sound
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if ((firstBody.categoryBitMask & CollisionTypePlayer) != 0 && (secondBody.categoryBitMask & CollisionTypeExit) != 0)
    {
        // Player reached exit
        NSLog(@"Player reached exit");
        
        [self exitLevel];
    }
    
    if ((firstBody.categoryBitMask & CollisionTypePlayer) != 0 && (secondBody.categoryBitMask & CollisionTypeDoor) != 0)
    {
        // Player reached door
        NSLog(@"Player reached door");
        
        NSLog(@"node %@", secondBody.node);
        [secondBody setCategoryBitMask:0];
        
        [self openDoor];
    }
    
    if ((firstBody.categoryBitMask & CollisionTypeEnemy) != 0 && (secondBody.categoryBitMask & CollisionTypeSpell) != 0)
    {
        // Player reached door
        NSLog(@"Enemy hit by spell");
        
        ADCEnemy *enemy = (ADCEnemy *)firstBody.node;
        [enemy receiveDamage:2.0f];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        location = [self convertPoint:location toNode:self.map];
        [self.map enumerateChildNodesWithName:@"item" usingBlock:^(SKNode *node, BOOL *stop) {
            ADCItem *item = (ADCItem *)node;
            
            if ([item containsPoint:location] && item.canBePickedUp) {
                [self setPaused:YES];
                [self didPickUpItem:item];
                [item removeFromParent]; // TODO: handle picking up items that are stacked on each other.
            }
        }];
    }
}

- (void)didPickUpItem:(ADCItem *)item
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"itemPickup" object:item];
}

@end
