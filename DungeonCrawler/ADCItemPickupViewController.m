//
//  ADCItemPickupViewController.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCItemPickupViewController.h"
#import "ADCItem.h"
#import "ADCModelManager.h"
#import "User.h"
#import "Player.h"
#import "Weapon.h"
#import "Sheild.h"
#import "Helm.h"

@interface ADCItemPickupViewController ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *classLabel;
@property (nonatomic, weak) IBOutlet UITextView *statsView;
@property (nonatomic, weak) IBOutlet UIButton *sellButton;
@property (nonatomic, weak) IBOutlet UIImageView *weaponImageView;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Player *player;

@end

@implementation ADCItemPickupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.nameLabel setText:self.itemPickedUp.nameOfItem];
    
    NSString *name = [ADCItem typeNameFromClassType:self.itemPickedUp.classType];
    [self.classLabel setText:name];
    
    [self.sellButton setTitle:[NSString stringWithFormat:@"Sell for: %d gold", (int)self.itemPickedUp.sellValue] forState:UIControlStateNormal];
    
    NSInteger vitality = self.itemPickedUp.vitality;
    NSInteger damage = self.itemPickedUp.damage;
    NSInteger attackRate = self.itemPickedUp.attackRate;
    NSInteger defense = self.itemPickedUp.defense;
    
    // Get player stats
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    self.user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    NSNumber *playerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerId"];
    self.player = [[ADCModelManager sharedManager] playerWithId:playerId.integerValue forUser:self.user];
    
    NSInteger currentVitality = 0;
    NSInteger currentDamage = 0;
    CGFloat currentAttackRating = 0;
    NSInteger currentDefense = 0;
    
    // Get the stats of the player's current item based on the new item type.
    switch (self.itemPickedUp.classType) {
        case WEAPON:
            currentDamage = self.player.weapon.strength.integerValue;
            currentAttackRating = self.player.weapon.attack_rate.floatValue;
            currentVitality = self.player.weapon.vitality.integerValue;
            break;
            
        case OFF_HAND:
            currentDefense = self.player.shield.defense.integerValue;
            currentVitality = self.player.shield.vitality.integerValue;
            break;
            
        case HEAD_GEAR:
            currentDefense = self.player.helm.defense.integerValue;
            currentVitality = self.player.helm.vitality.integerValue;
            break;
            
        default:
            break;
    }
    
    NSString *damageString = (damage == 0) ? @"" : [NSString stringWithFormat:@"Damage: %d", (int)damage];
    NSString *attackRateString = (damage == 0) ? @"" : [NSString stringWithFormat:@"Attack Rate: %d", (int)attackRate];
    NSString *defenseString = (defense == 0) ? @"" : [NSString stringWithFormat:@"Defense: %d", (int)defense];
    NSString *vitalityString = (vitality == 0) ? @"" : [NSString stringWithFormat:@"Vitality: %d", (int)vitality];
    
    // Show the item comparison next to the new item.
    if (damageString.length > 0)
        damageString = [self statStringFromString:damageString withCurrentStat:currentDamage andNewStat:damage];
    
    if (attackRateString.length > 0)
        attackRateString = [self statStringFromString:attackRateString withCurrentStat:currentAttackRating andNewStat:attackRate];
    
    if (defenseString.length > 0)
        defenseString = [self statStringFromString:defenseString withCurrentStat:currentDefense andNewStat:defense];
    
    if (vitalityString.length > 0)
        vitalityString = [self statStringFromString:vitalityString withCurrentStat:currentVitality andNewStat:vitality];
    
    NSString *statsString = [NSString stringWithFormat:@"%@%@%@%@", damageString, attackRateString, defenseString, vitalityString];
    [self.statsView setText:statsString];
    
    NSString *textureName = [ADCItem typeNameFromClassType:self.itemPickedUp.classType];
    [self.weaponImageView setImage:[UIImage imageNamed:textureName]];
}

- (NSString *)statStringFromString:(NSString *)string withCurrentStat:(NSInteger)currentStat andNewStat:(NSInteger)newStat
{
    int adjustment = (int)(newStat - currentStat);
    NSString *adjString = adjustment > 0 ? [NSString stringWithFormat:@"+%d", adjustment] : [NSString stringWithFormat:@"%d", adjustment];
    return [NSString stringWithFormat:@"%@ | %@\n", string, adjString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unpause" object:nil];
}

- (IBAction)sellItem:(id)sender
{
    NSLog(@"Sold for %d", (int)self.itemPickedUp.sellValue);
    
    // Update player gold.
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    User *user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];

    [user setGoldCount:[NSNumber numberWithInteger:user.goldCount.integerValue + self.itemPickedUp.sellValue]];
    [[ADCModelManager sharedManager] saveContext];
    
    [self dismissViewController:nil];
}

- (IBAction)equipItem:(id)sender
{
    NSLog(@"item equiped");
    
    // Sell current item.
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    User *user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    NSNumber *playerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerId"];
    Player *player = [[ADCModelManager sharedManager] playerWithId:playerId.integerValue forUser:user];
    [user setGoldCount:[NSNumber numberWithInteger:user.goldCount.integerValue + player.weapon.value.integerValue]];
    
    // Update player item stats
    switch (self.itemPickedUp.classType) {
        case WEAPON:
            [[ADCModelManager sharedManager] setWeapon:self.itemPickedUp forPlayer:self.player];
            break;
            
        case OFF_HAND:
            [[ADCModelManager sharedManager] setShield:self.itemPickedUp forPlayer:self.player];
            break;
            
        case HEAD_GEAR:
            [[ADCModelManager sharedManager] setHelm:self.itemPickedUp forPlayer:self.player];
            break;
            
        default:
            break;
    }
    
    [self dismissViewController:nil];
}

@end
