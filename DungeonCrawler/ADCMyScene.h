//
//  ADCMyScene.h
//  DungeonCrawler
//

//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class Player;

typedef enum {
    CollisionTypePlayer = 0x1 << 0,
    CollisionTypeWall   = 0x1 << 1,
    CollisionTypeExit   = 0x1 << 2,
    CollisionTypeDoor   = 0x1 << 3,
    CollisionTypeEnemy  = 0x1 << 4,
    CollisionTypeSpell  = 0x1 << 5
} kCollisionType;

@interface ADCMyScene : SKScene

@property (nonatomic, strong) Player *savedPlayer;

@end
