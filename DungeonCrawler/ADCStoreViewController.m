//
//  ADCStoreViewController.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/28/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCStoreViewController.h"
#import "ADCModelManager.h"
#import "User.h"

// In-app purchases
#define kTeir1 @"com.potion.goldTier1"
#define kTeir2 @"com.potion.goldTier2"
#define kTeir3 @"com.potion.goldTier3"

#define kTier1Gold 10000
#define kTier2Gold 40000
#define kTier3Gold 100000

#define kFlamePotionCost 10000

typedef enum {
    FLAME_POTION = 0
} PotionTypes;

@interface ADCStoreViewController ()

@property (nonatomic, weak) IBOutlet UILabel *goldLabel;
@property (nonatomic, weak) IBOutlet UIButton *tier1Button;
@property (nonatomic, weak) IBOutlet UIButton *tier2Button;
@property (nonatomic, weak) IBOutlet UIButton *tier3Button;
@property (nonatomic, weak) IBOutlet UIButton *flameSkillPurchaseButton;

// In-app purchase
@property (nonatomic, strong) SKProductsRequest *productRequest;
@property (nonatomic, strong) NSArray *validProductsInStore;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) User *user;

@end

@implementation ADCStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flameSkill"] isEqualToValue:@1]) {
        [self.flameSkillPurchaseButton setEnabled:NO];
    }
    
    [self getAvailableProducts];
    
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    self.user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %@", [self.user goldCount]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unpause" object:nil];
}

- (IBAction)buyPotion:(UIButton *)button
{
    switch ([button tag]) {
            
        case FLAME_POTION:
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flameSkill"] isEqualToNumber:@1]) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Flame skill has already been unlocked"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                [alertView show];
                
            } else {
                if (self.user.goldCount.integerValue >= kFlamePotionCost) {
                    [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue - kFlamePotionCost]];
                    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %@", [self.user goldCount]]];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:@"flameSkill"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Flame skill purchased"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
            
            break;
            
        default:
            break;
    }
    
    [[ADCModelManager sharedManager] saveContext];
}

- (IBAction)buyGold:(UIButton *)button
{
    [self purchaseProduct:[self.validProductsInStore objectAtIndex:button.tag]];
    
    [self.activityIndicator startAnimating];
}

#pragma mark - In-app purchase

- (void)getAvailableProducts
{
    // Adding activity indicator
    [self.view addSubview:self.activityIndicator];
    
    NSSet *productIdentifiers = [NSSet setWithObjects:kTeir1, kTeir2, kTeir3, nil];
    self.productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    self.productRequest.delegate = self;
    [self.productRequest start];
}

- (BOOL)canMakePurchase
{
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseProduct:(SKProduct*)product
{
    if ([self canMakePurchase]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Please enable in-app purchases on your device"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark StoreKit Delegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSUInteger count = [response.products count];
    if (count > 0) {
        self.validProductsInStore = response.products;
        
        for (SKProduct *validProduct in response.products) {
            if ([validProduct.productIdentifier isEqualToString:kTeir1]) {
                [self.tier1Button setTitle:validProduct.price.stringValue forState:UIControlStateNormal];
            }
            
            if ([validProduct.productIdentifier isEqualToString:kTeir2]) {
                [self.tier2Button setTitle:validProduct.price.stringValue forState:UIControlStateNormal];
            }
            
            if ([validProduct.productIdentifier isEqualToString:kTeir3]) {
                [self.tier3Button setTitle:validProduct.price.stringValue forState:UIControlStateNormal];
            }
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch (transaction.transactionState) {
                
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing Product From Store!");
                [self.activityIndicator startAnimating];
                break;
                
            case SKPaymentTransactionStatePurchased: {
                if ([transaction.payment.productIdentifier isEqualToString:kTeir1]) {
                    // 10,000
                    [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue + kTier1Gold]];
                    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %@", [self.user goldCount]]];
                }
                
                if ([transaction.payment.productIdentifier isEqualToString:kTeir2]) {
                    // 40,000
                    [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue + kTier2Gold]];
                    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %@", [self.user goldCount]]];
                }
                
                if ([transaction.payment.productIdentifier isEqualToString:kTeir3]) {
                    // 100,000
                    [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue + kTier3Gold]];
                    [self.goldLabel setText:[NSString stringWithFormat:@"Gold: %@", [self.user goldCount]]];
                }
                
                [[ADCModelManager sharedManager] saveContext];
                
                NSLog(@"Product purchased from store!");
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Purchase succesful"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles: nil];
                [alertView show];
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
                
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//                [self disableAds];
                break;
                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");

                [self.activityIndicator stopAnimating];
                break;
                
            default:
                break;
        }
    }
}

- (IBAction)checkPurchasedItems:(id)sender
{
    // TODO: disabled once restored
    [self.activityIndicator startAnimating];
    
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

//Then this delegate Function Will be fired
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{

}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"PaymentQueue error %@", error);
    
    [self.activityIndicator stopAnimating];
}

@end
