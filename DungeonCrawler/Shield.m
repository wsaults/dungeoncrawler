//
//  Shield.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Shield.h"
#import "Player.h"


@implementation Shield

@dynamic defense;
@dynamic item_name;
@dynamic value;
@dynamic vitality;
@dynamic player;

@end
