//
//  ADCMenuTableViewCell.h
//  DungeonCrawler
//
//  Created by Will Saults on 5/1/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADCMenuTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *heroImageView;
@property (nonatomic, strong) UILabel *deceasedLabel;
@property (nonatomic, strong) UILabel *tapToReviveLabel;


@end
