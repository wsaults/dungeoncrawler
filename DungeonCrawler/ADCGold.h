//
//  ADCGold.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/6/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "ADCLoot.h"

@interface ADCGold : ADCLoot

@property (nonatomic, assign) NSInteger quantity;

@end
