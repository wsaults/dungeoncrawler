//
//  ADCMenuViewController.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/22/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCMenuViewController.h"
#import "ADCModelManager.h"
#import "User.h"
#import "Player.h"
#import "ADCViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ADCMenuTableViewCell.h"

@interface ADCMenuViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *heros;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Player *selectedHero;

#pragma mark - Loading indicator
@property (nonatomic, weak) IBOutlet UIView *loadingView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) BOOL didCancelAction;

#pragma mark - Audio
@property (nonatomic, strong) AVAudioPlayer *backgroundMusicPlayer;

@end

@implementation ADCMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.didCancelAction = NO;
    
    [self authenticateLocalPlayer];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSError *error;
    NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"menu-bg" withExtension:@"wav"];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.backgroundMusicPlayer.numberOfLoops = -1;
    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer play];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"fadeMusic" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self fadeVolume];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"playMusic" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self.backgroundMusicPlayer play];
    }];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fadeVolume
{
    if (self.backgroundMusicPlayer.volume > 0.1) {
        self.backgroundMusicPlayer.volume = self.backgroundMusicPlayer.volume - 0.1;
        [self performSelector:@selector(fadeVolume) withObject:nil afterDelay:0.1];
    } else {
        // Stop and get the sound ready for playing again
        [self.backgroundMusicPlayer stop];
        self.backgroundMusicPlayer.currentTime = 0;
        [self.backgroundMusicPlayer prepareToPlay];
        self.backgroundMusicPlayer.volume = 1.0;
    }
}

#pragma mark - Tableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.heros != nil && self.heros.count > 0) ? self.heros.count + 1 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    ADCMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ADCMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:.20f]];
    }
    
    if (indexPath.row == self.heros.count) {
        [cell.textLabel setText:@"Add Hero"];
    } else {
        Player *hero = [self.heros objectAtIndex:indexPath.row];
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", hero.first_name, hero.last_name];
        [cell.textLabel setText:[NSString stringWithFormat:@"%@ - Lv:%@", fullName, hero.level]];
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"Floor:%@", hero.farthestFloorReached]];
        
        cell.heroImageView.hidden = NO;
        
        if (![hero.is_alive isEqual:@0] ? NO : YES) {
            // Display a desceased label and a revive button
            [cell.deceasedLabel setText:@"Deceased"];
            [cell.deceasedLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.50]];
            [cell.deceasedLabel setTextColor:[UIColor redColor]];
            [cell.deceasedLabel setTextAlignment:NSTextAlignmentCenter];
            [cell.deceasedLabel setFont:[UIFont boldSystemFontOfSize:24]];
            
            [cell.tapToReviveLabel setText:@"Tap to revive"];
            [cell.tapToReviveLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.50]];
            [cell.tapToReviveLabel setTextColor:[UIColor redColor]];
            [cell.tapToReviveLabel setTextAlignment:NSTextAlignmentCenter];
            [cell.tapToReviveLabel setFont:[UIFont systemFontOfSize:14]];
        } else {
            [cell.deceasedLabel setText:@""];
            [cell.tapToReviveLabel setText:@""];
            [cell.deceasedLabel setBackgroundColor:[UIColor clearColor]];
            [cell.tapToReviveLabel setBackgroundColor:[UIColor clearColor]];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == self.heros.count) {
        NSLog(@"add hero");
        [self.heros addObject:[[ADCModelManager sharedManager] addPlayerToUser:self.user]];
        [self.tableView reloadData];
    } else {
        
        Player *hero = [self.heros objectAtIndex:indexPath.row];
        if (![hero.is_alive isEqual:@0] ? NO : YES) {
            self.selectedHero = hero;
            
            // Show review view
            UIAlertView *reviveAlert = [[UIAlertView alloc] initWithTitle:@"Revive Hero - 5,000 Gold"
                                                                  message:@"Would you like to revive your hero?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [reviveAlert show];
            
            
        } else {
            // Loading animation with cancel button and delay.
            if (self.loadingView.isHidden) {
                [self.loadingView setHidden:NO];
                [self.activityIndicator startAnimating];
                self.didCancelAction = NO;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    if (self.didCancelAction == NO) {
                        
                        [[NSUserDefaults standardUserDefaults] setObject:hero.playerId forKey:@"playerId"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self hideLoadingView];
                        
                        [self fadeVolume];
                        [self performSegueWithIdentifier:@"gameSegue" sender:self];
                    }
                });
            }
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    ADCViewController *vc =
    [segue destinationViewController];
}

- (IBAction)cancelAction:(id)sender
{
    self.didCancelAction = YES;
    [self hideLoadingView];
}

- (void)hideLoadingView
{
    if (!self.loadingView.isHidden) {
        [self.loadingView setHidden:YES];
        [self.activityIndicator stopAnimating];
    }
}

#pragma mark - Gamecenter
- (void)authenticateLocalPlayer
{
    __weak GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil) {
        } else if (localPlayer.isAuthenticated) {
            // Called after the loacal player is authenticated.
            [self authenticatedPlayer:localPlayer];
        } else {
        }
    };
}

- (void)authenticatedPlayer:(GKLocalPlayer *)localPlayer
{
    NSLog(@"authenticatedPlayer");
    
    [[NSUserDefaults standardUserDefaults] setObject:[localPlayer playerID] forKey:@"playerGameCenterId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.user = [[ADCModelManager sharedManager] userWithGameCenterId:[localPlayer playerID]];
    
//    [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue + 20000]];
//    [[ADCModelManager sharedManager] saveContext];
    
    self.heros = [[NSMutableArray alloc] initWithArray:[[ADCModelManager sharedManager] playersForUser:self.user]];
    if (self.heros.count == 0) {
        [self.heros addObject:[[ADCModelManager sharedManager] addPlayerToUser:self.user]];
    }
    
    [self.tableView reloadData];
}

- (void)showGameCenter
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        [self presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            if (self.user.goldCount.integerValue >= 5000) {
                [self.user setGoldCount:[NSNumber numberWithInteger:self.user.goldCount.integerValue - 5000]];
                
                [self.selectedHero setIs_alive:[NSNumber numberWithBool:YES]];
                [[ADCModelManager sharedManager] saveContext];
                [self.tableView reloadData];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                      message:@"Insufficient gold"
                                                                     delegate:self
                                                            cancelButtonTitle:@"No" otherButtonTitles:nil, nil];
                [alert show];
            }
            break;
            
        default:
            break;
    }
}

@end
