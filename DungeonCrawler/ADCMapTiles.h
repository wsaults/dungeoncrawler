//
//  ADCMapTiles.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MapTileType)
{
    MapTileTypeInvalid  = -1,
    MapTileTypeNone     = 0,
    MapTileTypeFloor    = 1,
    MapTileTypeWall     = 2,
    MapTileTypeDoor     = 3,
};

@interface ADCMapTiles : NSObject

@property (nonatomic, readonly) NSUInteger count; // provides the total number of tiles in the grid
@property (nonatomic, readonly) CGSize gridSize;  // holds the width and height of the grid in tiles

- (instancetype)initWithGridSize:(CGSize)size;
- (MapTileType)tileTypeAt:(CGPoint)tileCoordinate;
- (void)setTileType:(MapTileType)type at:(CGPoint)tileCoordinate;
- (BOOL)isEdgeTileAt:(CGPoint)tileCoordinate;
- (BOOL)isValidTileCoordinateAt:(CGPoint)tileCoordinate;

@end
