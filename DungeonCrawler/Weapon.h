//
//  Weapon.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface Weapon : NSManagedObject

@property (nonatomic, retain) NSNumber * attack_rate;
@property (nonatomic, retain) NSNumber * strength;
@property (nonatomic, retain) NSNumber * vitality;
@property (nonatomic, retain) NSString * item_name;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) Player *player;

@end
