//
//  ADCPotion.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/27/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCLoot.h"

@interface ADCPotion : ADCLoot

@property (nonatomic, assign) NSInteger healthPoints;

@end
