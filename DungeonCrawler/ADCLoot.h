//
//  ADCLoot.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/7/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ADCLoot : SKSpriteNode

- (void)pickupAtPosition:(CGPoint)position;

@end
