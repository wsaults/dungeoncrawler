//
//  ADCItemDropManager.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/2/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCLootDropManager.h"
#import "ADCItem.h"
#import "ADCGold.h"
#import "ADCTextureManager.h"
#import "ADCUtilities.h"
#import "User.h"
#import "Player.h"
#import "ADCModelManager.h"
#import "ADCPotion.h"

@implementation ADCLootDropManager

+ (instancetype)sharedManager{
    static ADCLootDropManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (instancetype)init {
    if (self = [super init]) {
    }
    return self;
}

// Kindly ask the Item Drop Manager to bestow some item upon the player.
- (void)askForItemDropAtPosition:(CGPoint)location
{
    // Run a random number generator to decide if the item should drop.
    if (random_int(0, 10) == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ItemDrop" object:[NSValue valueWithCGPoint:location]];
    }
}

- (void)askForGoldDropAtPosition:(CGPoint)location
{
    if (random_int(0, 2) == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoldDrop" object:[NSValue valueWithCGPoint:location]];
    }
}

- (void)askForPotionDropAtPosition:(CGPoint)location
{
    if (random_int(0, 7) == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PotionDrop" object:[NSValue valueWithCGPoint:location]];
    }
}

- (ADCItem *)item
{
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    User *user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    Player *savedPlayer = [[ADCModelManager sharedManager] playerWithId:0 forUser:user];
    NSInteger lv = savedPlayer.level.integerValue;
    
    // Generate a random item.
    
    SKTextureAtlas *atlas = [[ADCTextureManager sharedManager] itemsAtlas];
    
    NSInteger itemType = random_int(0, 3);
    NSString *textureName = [ADCItem typeNameFromClassType:itemType];
    
    ADCItem *item = [[ADCItem alloc] initWithTexture:[atlas textureNamed:textureName] andClassType:itemType];
    [item generateItemStatsBasedOnPlayerLvl:lv];
    
    NSInteger itemPerceivedValue = item.vitality + item.defense + item.damage;
    
    [item setSellValue:random_int((itemPerceivedValue * 50) / 4, (itemPerceivedValue * 200) / 4)];
    
    return item;
}

- (ADCGold *)gold
{
    // Generate a gold object with a random value based on the player's level.
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    User *user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    Player *savedPlayer = [[ADCModelManager sharedManager] playerWithId:0 forUser:user];
    NSInteger lv = savedPlayer.level.integerValue;
    
    SKTextureAtlas *atlas = [[ADCTextureManager sharedManager] spriteAtlas];
    
    // Gold texture based on amount
    NSInteger amount = random_int(1 * lv, 4 * lv);
    NSInteger thirdOfMax = ((4 * lv) / 3) + 1;
    
    int textureId = 1;
    if ((amount >= thirdOfMax) && (amount <= thirdOfMax * 2)) {
        textureId = 2;
    } else if ((amount >= thirdOfMax * 2) && (amount <= thirdOfMax * 3)) {
        textureId = 3;
    }
    
    NSString *textureName = [NSString stringWithFormat:@"gold_%d", textureId];
    ADCGold *gold = [[ADCGold alloc] initWithTexture:[atlas textureNamed:textureName]];
    [gold setQuantity:amount];
    [gold setName:@"gold"];
    
    return gold;
}

- (ADCPotion *)potion
{
    // Generate a potion object with a random value based on the player's level.
    NSString *gcId = [[NSUserDefaults standardUserDefaults] objectForKey:@"playerGameCenterId"];
    User *user = [[ADCModelManager sharedManager] userWithGameCenterId:gcId];
    Player *savedPlayer = [[ADCModelManager sharedManager] playerWithId:0 forUser:user];
    NSInteger lv = savedPlayer.level.integerValue;
    
    SKTextureAtlas *atlas = [[ADCTextureManager sharedManager] spriteAtlas];
    
    ADCPotion *potion = [[ADCPotion alloc] initWithTexture:[atlas textureNamed:@"potion"]];
    [potion setHealthPoints:random_int(5 * lv, 7 * lv)];
    [potion setName:@"potion"];
    
    return potion;
}

@end
