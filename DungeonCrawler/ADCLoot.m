//
//  ADCLoot.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/7/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCLoot.h"

@implementation ADCLoot

- (void)pickupAtPosition:(CGPoint)position
{
    // Overridden by subclass
}

@end
