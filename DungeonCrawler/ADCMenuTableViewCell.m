//
//  ADCMenuTableViewCell.m
//  DungeonCrawler
//
//  Created by Will Saults on 5/1/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//≥

#import "ADCMenuTableViewCell.h"

@implementation ADCMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.heroImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"player"]];
        [self.heroImageView setFrame:CGRectMake(self.frame.size.width - 50, 5, self.frame.size.height - 10, self.frame.size.height - 10)];
        self.heroImageView.hidden = YES;
        
        self.deceasedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height * .66)];
        self.tapToReviveLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.deceasedLabel.frame.size.height, self.frame.size.width, self.frame.size.height * .33)];
        
        [self addSubview:self.heroImageView];
        [self addSubview:self.deceasedLabel];
        [self addSubview:self.tapToReviveLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
