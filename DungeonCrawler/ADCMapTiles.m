//
//  ADCMapTiles.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCMapTiles.h"

@interface ADCMapTiles ()
@property (nonatomic) NSInteger *tiles;
@end

@implementation ADCMapTiles

// TODO: Nearly all of this is from a Ray Wenderlich tutorial. Rethink how this is being done and do it a different way. Thanks Ray!

// ARC will not manage memory allocated using calloc. Release the memory whenever ADCMapTiles is deallocated
- (void) dealloc
{
    if (self.tiles)
    {
        free(self.tiles);
        self.tiles = nil;
    }
}

- (instancetype)initWithGridSize:(CGSize)size
{
    if ((self = [super init]))
    {
        _gridSize = size;
        _count = (NSUInteger) size.width * size.height;
        self.tiles = calloc(self.count, sizeof(NSInteger));
        NSAssert(self.tiles, @"Could not allocate memory for tiles");
    }
    return self;
}

- (MapTileType)tileTypeAt:(CGPoint)tileCoordinate
{
    NSInteger tileArrayIndex = [self tileIndexAt:tileCoordinate];
    if (tileArrayIndex == -1)
    {
        return MapTileTypeInvalid;
    }
    return self.tiles[tileArrayIndex];
}

- (void)setTileType:(MapTileType)type at:(CGPoint)tileCoordinate
{
    NSInteger tileArrayIndex = [self tileIndexAt:tileCoordinate];
    if (tileArrayIndex == -1)
    {
        return;
    }
    self.tiles[tileArrayIndex] = type;
}

- (BOOL)isValidTileCoordinateAt:(CGPoint)tileCoordinate
{
    return !(tileCoordinate.x < 0 ||
             tileCoordinate.x >= self.gridSize.width ||
             tileCoordinate.y < 0 ||
             tileCoordinate.y >= self.gridSize.height);
}

- (NSInteger)tileIndexAt:(CGPoint)tileCoordinate
{
    if ( ![self isValidTileCoordinateAt:tileCoordinate] )
    {
        NSLog(@"Not a valid tile coordinate at %@", NSStringFromCGPoint(tileCoordinate));
        return MapTileTypeInvalid;
    }
    
    // index in memory = y * gridSize.width + x
    return ((NSInteger)tileCoordinate.y * (NSInteger)self.gridSize.width + (NSInteger)tileCoordinate.x);
}

- (BOOL)isEdgeTileAt:(CGPoint)tileCoordinate
{
    return ((NSInteger)tileCoordinate.x == 0 ||
            (NSInteger)tileCoordinate.x == (NSInteger)self.gridSize.width - 1 ||
            (NSInteger)tileCoordinate.y == 0 ||
            (NSInteger)tileCoordinate.y == (NSInteger)self.gridSize.height - 1);
}

- (NSString *)description
{
    NSMutableString *tileMapDescription = [NSMutableString stringWithFormat:@"<%@ = %p | \n", [self class], self];
    
    for (int y = ((int)self.gridSize.height - 1); y >= 0; y--)
    {
        [tileMapDescription appendString:[NSString stringWithFormat:@"%@[%i]", y < 10 ? @" " : @"", y]];
        
        for (NSInteger x = 0; x < (NSInteger)self.gridSize.width; x++ )
        {
            [tileMapDescription appendString:[NSString stringWithFormat:@"%i", (int)[self tileTypeAt:CGPointMake(x, y)]]];
        }
        [tileMapDescription appendString:@"\n"];
    }
    return [tileMapDescription stringByAppendingString:@">"];
}

@end
