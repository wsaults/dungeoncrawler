//
//  Utilities.m
//
//  Created by Will Saults 
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#pragma mark - Point Calculations
CGFloat DistanceBetweenPoints(CGPoint first, CGPoint second)
{
    return hypotf(second.x - first.x, second.y - first.y);
}

//// Calculate the distance between two points.
//CGFloat dx = point2.x - point1.x;
//CGFloat dy = point2.y - point1.y;
//return sqrt(dx * dx + dy * dy);

CGFloat RadiansBetweenPoints(CGPoint first, CGPoint second)
{
    CGFloat deltaX = second.x - first.x;
    CGFloat deltaY = second.y - first.y;
    return atan2f(deltaY, deltaX);
}

CGPoint PointByAddingCGPoints(CGPoint first, CGPoint second)
{
    return CGPointMake(first.x + second.x, first.y + second.y);
}

#pragma mark - Emitters
void RunOneShotEmitter(SKEmitterNode *emitter, CGFloat duration)
{
    [emitter runAction:[SKAction sequence:@[
                                            [SKAction waitForDuration:duration],
                                            [SKAction runBlock:^{
                                                emitter.particleBirthRate = 0;
                                            }],
                                            [SKAction waitForDuration:emitter.particleLifetime + emitter.particleLifetimeRange],
                                            [SKAction removeFromParent],
                                            ]]];
}

void RunNumOfParticlesEmitter(SKEmitterNode *emitter, NSUInteger numOfParticles)
{
    [emitter runAction:[SKAction sequence:@[[SKAction runBlock:^{
                                            emitter.numParticlesToEmit = numOfParticles;
    }],
                                            [SKAction waitForDuration:emitter.particleLifetime + emitter.particleLifetimeRange],
                                            [SKAction removeFromParent],
                                            ]]];
}


#pragma mark - SKEmitterNode Category
@implementation SKEmitterNode (Additions)
+ (instancetype)emitterNodeWithEmitterNamed:(NSString *)emitterFileName
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:emitterFileName ofType:@"sks"]];
}
@end