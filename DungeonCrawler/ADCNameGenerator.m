//
//  ADCNameGenerator.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/29/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCNameGenerator.h"
#import "ADCUtilities.h"

@interface ADCNameGenerator ()

#define kFirstNames [NSMutableArray arrayWithObjects:@"Edwin", @"Bruce", @"Mac", nil]
#define kLastNames [NSMutableArray arrayWithObjects:@"McLondon", @"Armstrong", @"Fleetwood", nil]

@end

@implementation ADCNameGenerator

+ (instancetype)shared
{
    static ADCNameGenerator *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (instancetype)init
{
    if (self = [super init]) {
    }
    return self;
}

- (NSDictionary *)generateFullName
{
    int rand = (int)random_int(0, kFirstNames.count - 1);
    NSString *firstName = [kFirstNames objectAtIndex:rand];
    
    rand = (int)random_int(0, kLastNames.count - 1);
    NSString *lastName = [kLastNames objectAtIndex:rand];
    
    NSDictionary *nameDict = [[NSDictionary alloc] initWithObjectsAndKeys:firstName, kFirstName, lastName, kLastName,nil];
    return nameDict;
}

@end
