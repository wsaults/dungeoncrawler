//
//  ADCAppDelegate.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
