//
//  ADCPlayer.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCPlayer.h"
#import "ADCTextureManager.h"

@interface ADCPlayer ()

@property (nonatomic, getter = isPlayingMoveSound) BOOL playingMoveSound;
@property (nonatomic, strong) SKAction *playerAttack;
@property (nonatomic, strong) SKAction *playerMove;
@property (nonatomic, strong) SKAction *playerDied;

@end

@implementation ADCPlayer

- (void)die {
//    [self removeFromParent];
    
    if (!self.isDying) {
        self.dying = YES;
       
        self.playerDied = [SKAction playSoundFileNamed:@"player_death.mp3" waitForCompletion:NO];
        [self.scene runAction:self.playerDied];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayerDied" object:self];
    }
}

- (void)attackSound
{
    self.playerAttack = [SKAction playSoundFileNamed:@"player_attack.mp3" waitForCompletion:NO];
    [self.scene runAction:self.playerAttack];
}

- (void)attackAnimationTowardCharacter:(ADCCharacter *)character
{
    //        float deltaX = character.position.x - self.position.x;
    //        float deltaY = character.position.y - self.position.y;
    //        float angle = atan2f(deltaY, deltaX);
    
    SKSpriteNode *attackNode = [SKSpriteNode spriteNodeWithTexture:[[[ADCTextureManager sharedManager] attacksAtlas] textureNamed:@"sword"]];
    //        [attackNode setAlpha:0];
    [attackNode setPosition:self.position];
    //        [attackNode setAnchorPoint:CGPointMake(.5, 0)];
    //        [attackNode setZRotation:angle];
    [self.parent addChild:attackNode];
    
    SKAction *fade = [SKAction fadeAlphaTo:1 duration:.2];
    SKAction *move = [SKAction moveTo:character.position duration:.2];
    
    SKAction *group = [SKAction group:@[fade, move]];
    SKAction *block = [SKAction runBlock:^{
        [attackNode removeFromParent];
    }];
    SKAction *attackSequence = [SKAction sequence:@[group, block]];
    [attackNode runAction:attackSequence];
}

- (void)updatePosition:(CGPoint)position
{
    if (!CGPointEqualToPoint(position, self.position) && self.isDying == NO) {
        [self setPosition:position];
        [self moveSound];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMiniMapPosition" object:[NSValue valueWithCGPoint:position]];
    }
}

- (void)moveSound
{
    if (!self.playingMoveSound) {
        self.playingMoveSound = YES;
        
        self.playerMove = [SKAction playSoundFileNamed:@"player_move.wav" waitForCompletion:YES];
        [self.scene runAction:self.playerMove completion:^{
            self.playingMoveSound = NO;
        }];
    }
}

@end
