//
//  ADCEnemy.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCCharacter.h"

@interface ADCEnemy : ADCCharacter

@property (nonatomic, getter = isAlive) BOOL alive;
@property (nonatomic, assign) CGFloat expValue;
@property (nonatomic, assign) int enemyType;

- (instancetype)initWithPosition:(CGPoint)position withType:(int)type;

- (void)startChase;
- (void)stopChase;

@end
