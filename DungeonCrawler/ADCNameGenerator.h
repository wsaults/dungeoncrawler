//
//  ADCNameGenerator.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/29/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kFirstName @"firstName"
#define kLastName @"lastName"

@interface ADCNameGenerator : NSObject

+ (instancetype)shared;

- (NSDictionary *)generateFullName;

@end
