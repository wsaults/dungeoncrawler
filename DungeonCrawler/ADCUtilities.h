//
//  Utilities.h
//
//  Created by Will Saults
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

/* Generate a random float between 0.0f and 1.0f. */
#define RANDOM_0_1() (arc4random() / (float)(0xffffffffu))

/* Generate a random float between two numbers */
#define boris_random(min, max) ((((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * (max - min)) + min)

/* Generate a random integer between two numbers */
#define random_int(min, max) ((NSInteger) (min + arc4random() % (max - min)))

//- (NSInteger)randomNumberBetweenMin:(NSInteger)min andMax:(NSInteger)max
//{
//    return min + arc4random() % (max - min);
//}

/* Distance and coordinate utility functions. */
CGFloat DistanceBetweenPoints(CGPoint first, CGPoint second);
CGFloat RadiansBetweenPoints(CGPoint first, CGPoint second);
CGPoint PointByAddingCGPoints(CGPoint first, CGPoint second);

/* Run the given emitter once, for duration. */
void RunOneShotEmitter(SKEmitterNode *emitter, CGFloat duration);

/* Run the given emitter over x number of particles. */
void RunNumOfParticlesEmitter(SKEmitterNode *emitter, NSUInteger numOfParticles);

/* Category on SKEmitterNode to make it easy to load an emitter from a node file created by Xcode. */
@interface SKEmitterNode (Additions)
+ (instancetype)emitterNodeWithEmitterNamed:(NSString *)emitterFileName;
@end