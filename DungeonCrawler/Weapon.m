//
//  Weapon.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Weapon.h"
#import "Player.h"


@implementation Weapon

@dynamic attack_rate;
@dynamic strength;
@dynamic vitality;
@dynamic item_name;
@dynamic value;
@dynamic player;

@end
