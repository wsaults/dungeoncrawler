//
//  ADCModelManager.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/6/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;
@class Player;
@class ADCItem;

@interface ADCModelManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedManager;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (NSManagedObject *)createEntity:(NSString*)entity;

#pragma mark - User
- (User *)createUserWithGameCenterId:(NSString *)gameCenterId;
- (User *)userWithGameCenterId:(NSString *)gameCenterId;

#pragma mark - Player
- (Player *)addPlayerToUser:(User *)user;
- (NSArray *)playersForUser:(User *)user;
- (Player *)playerWithId:(NSUInteger)playerId forUser:(User *)user;
- (void)levelUpPlayer:(Player *)player;

#pragma mark - Weapon
- (void)setWeapon:(ADCItem *)item forPlayer:(Player *)player;

#pragma mark - Helm
- (void)setHelm:(ADCItem *)item forPlayer:(Player *)player;

#pragma mark - Off hand
- (void)setShield:(ADCItem *)item forPlayer:(Player *)player;


@end
