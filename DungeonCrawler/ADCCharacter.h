//
//  ADCCharacter.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ADCCharacter : SKSpriteNode

@property (nonatomic, getter = isAttacking) BOOL attacking; // Know if a character is attacking.

// Attributes
@property (nonatomic, assign) CGFloat vitality;     // If it runs out, you die.
@property (nonatomic, assign) CGFloat defense;      // defense reduces the amount of damage the character takes from an attack.
@property (nonatomic, assign) CGFloat damage;       // The amount of damage the character can dish out. Combines strength and weapon damage.
@property (nonatomic, assign) CGFloat strength;     // Increased amount of damage the character can dish out.
@property (nonatomic, assign) CGFloat attackRate;   // How often the character swings their weapon.

// Movement
@property (nonatomic) CGFloat movementSpeed; // How fast the character moves across the world.

- (void)attackCharacter:(ADCCharacter *)character;
- (void)attackSound;
- (void)attackAnimationTowardCharacter:(ADCCharacter *)character;
- (void)moveSound;
- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval;
- (void)die;
- (void)receiveDamage:(CGFloat)damage;

@end
