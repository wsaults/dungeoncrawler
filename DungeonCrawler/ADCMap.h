//
//  ADCMap.h
//  DungeonCrawler
//
//  Created by Will Saults on 3/21/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class ADCMapTiles;
@class ADCMyScene;

@interface ADCMap : SKNode

@property (nonatomic, assign) CGSize gridSize;
@property (nonatomic, readonly) CGPoint spawnPoint;
@property (nonatomic, readonly) CGPoint exitPoint;
@property (nonatomic, strong) ADCMapTiles *tiles;
@property (nonatomic, strong) SKTextureAtlas *spriteAtlas;
@property (nonatomic, strong) NSMutableArray *enemies;

+ (instancetype)mapWithGridSize:(CGSize)gridSize;
- (instancetype)initWithGridSize:(CGSize)gridSize;
- (void)generateLevel:(NSUInteger)worldLevel;

@end
