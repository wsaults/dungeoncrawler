//
//  ADCStoreViewController.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/28/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface ADCStoreViewController : UIViewController <SKPaymentTransactionObserver, SKProductsRequestDelegate>

@end
