//
//  User.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/29/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "User.h"
#import "Player.h"


@implementation User

@dynamic goldCount;
@dynamic gameCenterId;
@dynamic players;

@end
