//
//  Sheild.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/28/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Sheild.h"
#import "Player.h"


@implementation Sheild

@dynamic defense;
@dynamic item_name;
@dynamic value;
@dynamic vitality;
@dynamic player;

@end
