//
//  ADCItem.m
//  DungeonCrawler
//
//  Created by Will Saults on 3/30/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "ADCItem.h"
#import "ADCUtilities.h"

@implementation ADCItem

- (instancetype)initWithTexture:(SKTexture *)texture andClassType:(NSInteger)classType
{
    self = [super initWithTexture:texture];
    
    if (self) {
        _classType = classType;
        _canBePickedUp = NO;
        self.name = @"item";
    }
    
    return self;
}

- (void)generateItemStatsBasedOnPlayerLvl:(NSInteger)lv
{
    // Stats are based off of the player's level.
    
    // TODO: item name is based off of highest stat.
    
    NSInteger statBonus = random_int(lv, lv + 3);
    NSInteger vitalityBonus = 0;
    if (random_int(0, 4) == 1) {
        vitalityBonus = random_int(0, random_int(1, lv * 4));
    }
    
    switch (_classType) {
        case WEAPON:
            _damage = statBonus;
            _vitality = vitalityBonus;
            _attackRate = boris_random(.1, .4);
            _nameOfItem = @"Rusty Sword";
            
            if (_vitality > 0)
                _nameOfItem = [NSString stringWithFormat:@"%@ %@", _nameOfItem, @"of Life"];
            
            break;
            
        case OFF_HAND:
            _defense = statBonus;
            _vitality = vitalityBonus;
            _nameOfItem = @"Rusty shield";
            
            if (_vitality > 0)
                _nameOfItem = [NSString stringWithFormat:@"%@ %@", _nameOfItem, @"of Life"];
            break;
            
        case HEAD_GEAR:
            _defense = statBonus;
            _vitality = vitalityBonus;
            _nameOfItem = @"Rusty Helm";
            
            if (_vitality > 0)
                _nameOfItem = [NSString stringWithFormat:@"%@ %@", _nameOfItem, @"of Life"];
            break;
            
        default:
            break;
    }
}

+ (NSString *)typeNameFromClassType:(NSInteger)classType
{
    NSString *name = @"";
    switch (classType) {
        case WEAPON:
            name = @"weapon";
            break;
            
        case OFF_HAND:
            name = @"off_hand";
            break;
            
        case HEAD_GEAR:
            name = @"head_gear";
            break;
            
        default:
            break;
    }
    return name;
}

@end
