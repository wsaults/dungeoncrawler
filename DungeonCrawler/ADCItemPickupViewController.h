//
//  ADCItemPickupViewController.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADCItem;

@interface ADCItemPickupViewController : UIViewController

@property (nonatomic, strong) ADCItem *itemPickedUp;

@end
