//
//  Weapon.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/7/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Weapon.h"


@implementation Weapon

@dynamic strength;
@dynamic attack_rate;

@end
