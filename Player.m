//
//  Player.m
//  DungeonCrawler
//
//  Created by Will Saults on 4/10/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "Player.h"
#import "Helm.h"
#import "Sheild.h"
#import "User.h"
#import "Weapon.h"


@implementation Player

@dynamic attack_rate;
@dynamic defense;
@dynamic experiance;
@dynamic level;
@dynamic movement_speed;
@dynamic playerId;
@dynamic strength;
@dynamic vitality;
@dynamic user;
@dynamic weapon;
@dynamic sheild;
@dynamic helm;

@end
