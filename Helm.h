//
//  Helm.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/7/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Helm : NSManagedObject

@property (nonatomic, retain) NSNumber * defense;

@end
