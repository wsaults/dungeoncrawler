//
//  Weapon.h
//  DungeonCrawler
//
//  Created by Will Saults on 4/7/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Weapon : NSManagedObject

@property (nonatomic, retain) NSNumber * strength;
@property (nonatomic, retain) NSNumber * attack_rate;

@end
